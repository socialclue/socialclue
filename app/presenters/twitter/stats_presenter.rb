class Twitter::StatsPresenter

  def initialize(data)
    @top_url_analysis = Twitter::TopUrls.new data
    @top_hashtag_analysis = Twitter::TopHashtags.new data
    @top_usermentions_analysis = Twitter::TopUserMentions.new  data
    @top_sources_analysis = Twitter::TopSources.new data
    @top_retweets_analysis = Twitter::TopRetweets.new data
    @top_favorites_analysis = Twitter::TopFavorites.new data
    @top_timezones_analysis = Twitter::TopTimezones.new data
  end

  def top_hashtags
    @top_hashtag_analysis.top_results
  end

  def top_urls
    @top_url_analysis.top_results
  end

  def top_user_mentions
    @top_usermentions_analysis.top_results
  end

  def top_sources
    @top_sources_analysis.top_results
  end

  def top_retweets
    @top_retweets_analysis.top_results.reverse[0..5]
  end

  def top_favorites
    @top_favorites_analysis.top_results.reverse[0..5]
  end

  def top_timezones
    @top_timezones_analysis.top_results
  end

end


