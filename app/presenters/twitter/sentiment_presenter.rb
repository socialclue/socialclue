class Twitter::SentimentPresenter

  def initialize(data)
    @sentiment_analysis = Twitter::SentimentAnalysis.new data
  end

  def check_for_empty_search_results
    @sentiment_analysis.tweet_sentiment_matches
  end

  def tweets
    @sentiment_analysis.tweet_body
  end

  def total_count
    @sentiment_analysis.tweet_body.size
  end

  def count_positive
    @sentiment_analysis.tweet_count_positive
  end

  def count_negative
    @sentiment_analysis.tweet_count_negative
  end

  def percent_positive
    @sentiment_analysis.percent_positive
  end

  def percent_negative
    @sentiment_analysis.percent_negative
  end
end
