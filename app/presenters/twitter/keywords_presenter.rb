class Twitter::KeywordsPresenter

  def initialize(data)
    @analytics_barchart = Twitter::AnalyticsBarchart.new data
  end

  def data_for_barchart
    @analytics_barchart.find_keywords[1..30]
  end

end
