class Facebook::KeywordsPresenter

  def initialize graph_connections
    @graph_connections = graph_connections
    nodes
    keywords_analysis nodes
  end

  def data_for_barchart
    @analytics_barchart.find_keywords[1..30]
  end

  private

  def nodes
    @graph_connections.map do |node|
      {
        message: node['message'].presence || 'no message given',
        link: node['link'].presence || 'no link given',
        type: node['type'].presence || 'no type given',
        status_type: node['status_type'].presence || 'no status type given',
        created: node['created_time'].presence || 'no created time given',
        shares: node['shares'].try(:fetch, 'count').presence || '0',
        application: node['application'].try(:fetch, 'name').presence || 'no data given',
        interact: node['actions'].try(:first).try(:fetch, 'link').presence || 'no actions given'
      }
    end
  end

  def keywords_analysis nodes
    @analytics_barchart = Facebook::KeywordAnalysis.new nodes
  end

end
