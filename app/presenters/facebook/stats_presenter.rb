class Facebook::StatsPresenter

  def initialize graph_connections
    @graph_connections = graph_connections
    nodes
    @most_shared = Facebook::MostShared.new nodes
    @type_of_status_frequency = Facebook::TypeFrequency.new nodes
    @status_type_frequency = Facebook::StatusTypeFrequency.new nodes
    @application_type_frequency = Facebook::ApplicationFrequency.new nodes
  end

  def most_shared
    @most_shared.top_results.reverse[0..10]
  end

  def type_of_status_frequency
    @type_of_status_frequency.top_results
  end

  def status_type_frequency
    @status_type_frequency.top_results
  end

  def application_type_frequency
    @application_type_frequency.top_results
  end

  private

  def nodes
    @graph_connections.map do |node|
      {
        message: node['message'].presence || 'no message given',
        link: node['link'].presence || 'no link given',
        type: node['type'].presence || 'no type given',
        status_type: node['status_type'].presence || 'no status type given',
        created: node['created_time'].presence || 'no created time given',
        shares: node['shares'].try(:fetch, 'count').presence || '0',
        application: node['application'].try(:fetch, 'name').presence || 'no user app data given',
        interact: node['actions'].try(:first).try(:fetch, 'link').presence || 'no actions given'
      }
    end
  end

end
