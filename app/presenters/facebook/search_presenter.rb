class Facebook::SearchPresenter

  def initialize graph_search_results
    @graph_search_results = graph_search_results
  end

  def nodes
    @graph_search_results.each_with_object({}) do |node, hash|
      hash[node['id']] = [[node['name']], [node['category']], [node['location']], [node['start_time']]]
    end
  end

  def present_nodes
    nodes.each_with_object({}) do |(k,v), hash|
      hash[k] = v.flatten.reject &:blank?
    end
  end

end
