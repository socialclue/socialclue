class Facebook::FeedPresenter

  def initialize graph_connections
    @graph_connections = graph_connections
  end

  def sentiment_analysis
    Facebook::SentimentAnalysis.new(nodes)
  end

  def message_sentiment
    sentiment_analysis.message_sentiment
  end

  def total_count
    message_sentiment.size
  end

  def count_positive
    sentiment_analysis.message_count_positive
  end

  def count_negative
    sentiment_analysis.message_count_negative
  end

  def percent_positive
    sentiment_analysis.percent_positive
  end

  def percent_negative
    sentiment_analysis.percent_negative
  end

  private

  def nodes
    @graph_connections.map do |node|
      {
        message: node['message'].presence || 'no message given',
        link: node['link'].presence || 'no link given',
        type: node['type'].presence || 'no type given',
        status_type: node['status_type'].presence || 'no status type given',
        created: node['created_time'].presence || 'no created time given',
        shares: node['shares'].try(:fetch, 'count').presence || '0',
        application: node['application'].try(:fetch, 'name').presence || 'no data given',
        interact: node['actions'].try(:first).try(:fetch, 'link').presence || 'no actions given'
      }
    end
  end

end
