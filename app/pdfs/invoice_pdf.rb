class InvoicePdf < Prawn::Document

  def initialize(invoice, view)
    super()
    @invoice = invoice
    @view = view
    company_details
    invoice_details
    subscription_date_and_description
    subscription_details
    regards_message
  end

  def company_details
    move_down 30
    text "SocialClue", :size => 14, style: :bold
    move_down 6
    text "info@socialclue.io", :size => 12, style: :bold
  end

  def invoice_details
    move_down 40
    text "Date: #{@invoice.created_at.strftime("%d/%m/%Y")}", :size => 13
    move_down 10
    text "Invoice #: #{@invoice.id}", :size => 13
    move_down 10
    text "Bill to: #{@invoice.user.email}", :size => 13
  end

  def subscription_date_and_description
    move_down 40
    text  "Description: Standard Monthly Plan x 1", :size => 13
    move_down 10
    text "Subscription start date: #{@invoice.start_date.strftime("%d/%m/%Y")} ", :size => 13
    move_down 10
    text "Subscription end date : #{@invoice.end_date.strftime("%d/%m/%Y")}", :size => 13
  end

  def subscription_details
    move_down 40
    text "Subtotal: #{format_stripe_amount(@invoice.amount)} USD", :size => 13
    move_down 10
    text "Tax: $0.00 USD", :size => 13
    move_down 10
    text "Shipping: $0.00 USD", :size => 13
    move_down 10
    text "Total: #{format_stripe_amount(@invoice.amount)} USD", :size => 13
    move_down 10
    text "Amount Paid: #{format_stripe_amount(@invoice.amount)} USD", :size => 13
    move_down 10
    text "Balance Due: $0.00 USD", :size => 13
    move_down 20
    text "*EU customers: prices exclude VAT", :size => 13
  end

  def format_stripe_amount(amount)
    sprintf('$%0.2f', amount.to_f/100).gsub(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1,")
  end

  def regards_message
    move_down 40
    text "Thank You,", :size => 13
    move_down 6
    text "The SocialClue Team", :size => 14, style:  :bold
  end
end
