class DashboardController < ApplicationController

  before_filter :authenticate_user!
  before_filter :authenticate_active_user

  def index
  end

end
