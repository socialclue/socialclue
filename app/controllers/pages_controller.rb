class PagesController < ApplicationController

  def about
  end

  def learn_more
  end

  def privacy_policy
  end

  def terms_of_service
  end

  def pricing
  end

end
