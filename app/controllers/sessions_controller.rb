class SessionsController < ApplicationController
  before_filter :authenticate_user!
  before_filter :authenticate_active_user

  def create_twitter
    auth = request.env["omniauth.auth"]
    account = TwitterAccount.find_by_provider_and_uid(auth["provider"], auth["uid"]) || TwitterAccount.create_with_omniauth(auth)
    session[:twitter_account_id] = account.id
    account.update_attributes(user: current_user.email) if current_user
    redirect_to start_twitter_analytics_path
  end

  def create_facebook
    facebook_account = FacebookAccount.from_omniauth(env["omniauth.auth"])
    session[:facebook_account_id] = facebook_account.id
    auth = request.env["omniauth.auth"]
    account = FacebookAccount.find_by_provider_and_uid(auth["provider"], auth["uid"])
    account.update_attributes(user: current_user.email) if current_user
    redirect_to start_facebook_analytics_path
  end

  def destroy
    session[:twitter_account_id] = nil
    session[:facebook_account_id] = nil
    redirect_to root_url
  end

end
