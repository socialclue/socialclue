class Facebook::FacebookController < ApplicationController

  before_filter :authenticate_user!
  before_filter :authenticate_active_user
  helper_method :current_facebook_account

  def initialize_graph_api
    Koala::Facebook::API.new(oauth_token)
  end

  def oauth_token
    current_facebook_account.oauth_token
  end

end
