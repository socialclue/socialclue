class Facebook::AnalyticsController < Facebook::FacebookController

  before_filter :auth_facebook_account
  before_filter :initialize_graph_api

  def start
  end

  def search
    @graph_data = Facebook::SearchPresenter.new(graph_search_results).present_nodes
    fb_search_params
  end

  def feed
    @graph_feed = Facebook::FeedPresenter.new(graph_feed_connections)
  end

  def stats
    @fb_stats = Facebook::StatsPresenter.new(graph_feed_connections)
  end

  def keywords
    @fb_keywords = Facebook::KeywordsPresenter.new(graph_feed_connections)
  end

  private

  def fb_search_params
    session[:fb_search_query] = nil
    session[:fb_search_type] = nil
    session[:fb_search_query] = params[:q]
    session[:fb_search_type] = params[:type]
  end

  def graph
    initialize_graph_api
  end

  def auth_facebook_account
    redirect_to "/auth/facebook" unless current_facebook_account
  end

  def graph_search_results
    graph.search(params[:q], {type: params[:type]})
  end

  def graph_feed_connections
    if params[:type] == 'page'
      graph.get_connections(params[:id], "posts")
    else
      graph.get_connections(params[:id], "feed")
    end
  end

end
