class Twitter::InfoController < Twitter::TwitterController
  before_filter :auth_twitter_account

  def info
  end

end
