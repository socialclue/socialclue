class Twitter::AnalyticsController < Twitter::TwitterController

  before_filter :perform_twitter_search, only: [:sentiment, :stats, :keywords]
  before_filter :auth_twitter_account

  def start
  end

  def sentiment
    @search_query = params[:q]
    @sentiment_analysis = Twitter::SentimentPresenter.new @twitter_search_analytics
  end

  def stats
    @search_query = params[:q]
    @stats_analysis = Twitter::StatsPresenter.new @twitter_search_analytics
  end

  def keywords
    @search_query = params[:q]
    @keywords_analysis = Twitter::KeywordsPresenter.new @twitter_search_analytics
  end

end







