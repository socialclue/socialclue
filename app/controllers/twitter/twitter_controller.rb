class Twitter::TwitterController < ApplicationController

  before_filter :authenticate_user!
  before_filter :authenticate_active_user
  helper_method :current_twitter_account

  private

  def perform_twitter_search
    if params[:latitude].present?
      geo_search_twitter
    elsif params[:q].present?
      normal_search
    else
      no_search_params_given
    end
  end

  def no_search_params_given
    @twitter_search_analytics = []
  end

  def geo_search_twitter
    @twitter_search_analytics = current_twitter_account.twitter.search(params[:q], :count => 1000, :result_type => 'mixed', :lang => "en", :geocode => "#{params[:latitude]},#{params[:longitude]},#{params[:radius]}", :include_entities => true).collect.map do |tweet|
      {
        text: (tweet.text).dup,
        name: tweet.user.name,
        username: tweet.user.screen_name,
        id: tweet.id,
        created_at: tweet.created_at,
        expanded_url: tweet.urls[0],
        hashtag: tweet.hashtags[0],
        user_mentions: tweet.user_mentions[0],
        source: tweet.source,
        result_type: tweet.metadata.result_type,
        location: tweet.user.location,
        description: (tweet.user.description).dup,
        followers_count: tweet.user.followers_count,
        friends_count: tweet.user.friends_count,
        time_zone: tweet.user.time_zone,
        statuses_count: tweet.user.statuses_count,
        tweets_count: tweet.user.tweets_count,
        retweet_count: tweet.retweet_count,
        favorite_count: tweet.favorite_count,
        favorited: tweet.favorited,
        retweeted: tweet.retweeted,
        profile_image: tweet.media,
      }
    end
  end

  def normal_search
    @twitter_search_analytics = current_twitter_account.twitter.search(params[:q], :count => 1000, :result_type => 'mixed', :lang => "en", :include_entities => true).collect.map do |tweet|
      {
        text: (tweet.text).dup,
        name: tweet.user.name,
        username: tweet.user.screen_name,
        id: tweet.id,
        created_at: tweet.created_at,
        expanded_url: tweet.urls[0],
        hashtag: tweet.hashtags[0],
        user_mentions: tweet.user_mentions[0],
        source: tweet.source,
        result_type: tweet.metadata.result_type,
        location: tweet.user.location,
        description: (tweet.user.description).dup,
        followers_count: tweet.user.followers_count,
        friends_count: tweet.user.friends_count,
        time_zone: tweet.user.time_zone,
        statuses_count: tweet.user.statuses_count,
        tweets_count: tweet.user.tweets_count,
        retweet_count: tweet.retweet_count,
        favorite_count: tweet.favorite_count,
        favorited: tweet.favorited,
        retweeted: tweet.retweeted,
        profile_image: tweet.media,
      }
    end
  end

  def auth_twitter_account
    redirect_to "/auth/twitter?use_authorize=true" unless current_twitter_account
  end

end
