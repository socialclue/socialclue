class Twitter::PhotosController < Twitter::TwitterController

  before_filter :authenticate_user!
  before_filter :current_twitter_account

  def search
    if current_twitter_account
      if params[:q].blank?
        no_params_given
      else
        twitter_photo_search_results
      end
    else
      redirect_to "/auth/twitter?use_authorize=true"
    end
  end

  private

  def no_params_given
    @twitter_search_data = []
  end

  def twitter_photo_search
    twitter_search_data = current_twitter_account.twitter.search(("#{params[:q]} pic.twitter.com"), :count => 1000, :result_type => 'mixed', :land => "en").collect.map do |tweet|
      {
        name: tweet.user.name,
        username: tweet.user.screen_name,
        id: tweet.id,
        media_url: tweet.media[0]
      }
    end
  end

  def twitter_photo_search_results
    @twitter_search_photos = twitter_photo_search.uniq {|tweet| tweet[:media_url].id if tweet[:media_url]}
  end

end



