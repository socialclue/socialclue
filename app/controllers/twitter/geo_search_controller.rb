class Twitter::GeoSearchController < Twitter::TwitterController

  before_filter :perform_twitter_search
  before_filter :auth_twitter_account

  def search
    @search_query = params[:q]
  end

  def sentiment
    @search_query = params[:q]
    @sentiment_analysis = Twitter::SentimentPresenter.new @twitter_search_analytics
  end

  def stats
    @search_query = params[:q]
    @sentiment_analysis = Twitter::SentimentPresenter.new @twitter_search_analytics
    @stats_analysis = Twitter::StatsPresenter.new @twitter_search_analytics
  end

  def keywords
    @search_query = params[:q]
    @sentiment_analysis = Twitter::SentimentPresenter.new @twitter_search_analytics
    @keywords_analysis = Twitter::KeywordsPresenter.new @twitter_search_analytics
  end

end
