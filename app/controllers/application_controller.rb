class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :current_twitter_account
  helper_method :current_facebook_account

  helper_method :authenticate_admin_user!
  helper_method :authenticate_active_user

  private

  def after_sign_in_path_for(resource)
    resource.active_subscription? ? dashboard_path : new_subscription_path
  end

  def current_twitter_account
    @current_twitter_account ||= TwitterAccount.find(session[:twitter_account_id]) if session[:twitter_account_id]
  end

  def current_facebook_account
    @current_facebook_account ||= FacebookAccount.find(session[:facebook_account_id]) if session[:facebook_account_id]
  end

  def authenticate_admin_user!
    redirect_to new_user_session_path unless current_user.try(:is_admin?)
  end

  def authenticate_active_user
    redirect_to new_subscription_path, :notice => "Please upgrade your account" unless current_user.active_subscription?
  end

end
