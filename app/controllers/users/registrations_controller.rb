class Users::RegistrationsController < Devise::RegistrationsController

  after_filter :start_free_trial, :only => :create

  def destroy
    redirect_to root_path, flash: { error: "You can't delete your account" }
  end

  protected

  def after_sign_up_path_for(resource)
    dashboard_path
  end

  def start_free_trial
    if current_user
      current_user.trial_start = Date.today
      current_user.trial_end = Date.today + 15.days
      current_user.save!
    else
      new_user_registration_path
    end
  end
end
