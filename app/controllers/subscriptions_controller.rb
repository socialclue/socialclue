class SubscriptionsController < ApplicationController
  before_filter :authenticate_user!

  def new
    if current_user.active_subscription?
      redirect_to dashboard_path, :notice => "You are already subscribed"
    else
      @subscription = current_user.subscription || current_user.build_subscription
    end
  end

  def create
    stripe_email = params[:stripeEmail]
    stripe_token = params[:stripeToken]

    @subscription = current_user.build_subscription(stripe_email: stripe_email, stripe_token: stripe_token)
    if @subscription.save
      redirect_to dashboard_path, :notice => "Thank you for subscribing!"
    else
      redirect_to root_path, :notice => "Oops, something went wrong. Please try again or contact support"
    end

  rescue Stripe::CardError => e
    @subscription.errors.add :base, e.message
    @subscription.stripe_token = nil
    render :action => :new

  rescue Stripe::StripeError => e
    logger.error e.message
    @subscription.errors.add :base, "There was a problem with your credit card"
    @subscription.stripe_token = nil
    render :action => :new
  end

  def edit
    @subscription = current_user.subscription
  end

  def update
    stripe_email = params[:stripeEmail]
    stripe_token = params[:stripeToken]
    @subscription = current_user.subscription

    @subscription.update_attributes(stripe_token: stripe_token, stripe_email: stripe_email)
    if @subscription.save
      redirect_to dashboard_path, :notice => "Success! Your card details have been updated."
    else
      render :action => :edit, :notice => "Oops, something went wrong. Please try again or contact support"
    end

  rescue Stripe::StripeError => e
    logger.error e.message
    @subscription.errors.add :base, "There was a problem with your credit card"
    @subscription.stripe_token = nil
    render :action => :edit
  end

  def cancel
    @subscription = current_user.subscription
    @subscription.cancel_stripe

    if @subscription.save
      redirect_to root_path, :notice => "Your subscription has been cancelled. Goodbye!"
    else
      redirect_to edit_user_registration_path, :notice => "Oops, something went wrong. Please try again or contact support"
    end

  rescue Stripe::StripeError => e
    logger.error e.message
    @subscription.errors.add :base, "There was a problem with your credit card"
    @subscription.stripe_token = nil
    redirect_to edit_user_registration_path
  end
end
