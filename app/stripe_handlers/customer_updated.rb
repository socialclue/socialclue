class CustomerUpdated
  def call(event)
    user = User.where("stripe_id = ?", event.data.object.id).first
    StripeMailer.delay.new_payment_information(user)
  end
end
