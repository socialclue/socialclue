class ChargeSucceeded
  def call(event)
    user = User.where("stripe_id = ?", event.data.object.customer).first
    user.subscription.update_attributes(active_from: Date.today, active_until: (Date.today + 1.month)) if user.subscription

    invoice = Invoice.new(user_id: user.id, amount: event.data.object.amount, stripe_reference_id: event.data.object.id, start_date: Date.today, end_date: (Date.today + 1.month))
    StripeMailer.delay.payment_received(user, event.data.object.amount, Date.today, (Date.today + 1.month)) if invoice.save
  end
end





