class SubscriptionDeleted
  def call(event)
    user = User.where("stripe_id = ?", event.data.object.customer).first
    user.subscription.update_attributes(active: false, active_until: Date.today) if user.subscription
    StripeMailer.delay.account_cancelled(user)
  end
end
