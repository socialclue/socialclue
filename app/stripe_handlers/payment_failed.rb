class PaymentFailed
  def call(event)
    user = User.where("stripe_id = ?", event.data.object.customer).first
    user.subscription.update_attributes(active_until: (Date.today + 3.days))
    StripeMailer.delay.payment_failed(user, event.data.object.amount, Date.today, (Date.today + 1.month))
  end
end





