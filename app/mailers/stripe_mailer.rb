class StripeMailer < ActionMailer::Base
  default from: "info@socialclue.io"

  def payment_received(user, amount, start_date, end_date)
    @user = user
    @amount = format_stripe_amount(amount)
    @start_date = start_date
    @end_date = end_date
    mail(:to => user.email, :subject => "Payment Received – Thank You")
  end

  def payment_failed(user, amount, start_date, end_date)
    @user = user
    @amount = format_stripe_amount(amount)
    @start_date = start_date
    @end_date = end_date
    mail(:to => user.email, :subject => "Payment to SocialClue Failed")
  end

  def new_payment_information(user)
    @user = user
    mail(:to => user.email, :subject => "Updated Payment Information")
  end

  def account_cancelled(user)
    @user = user
    mail(:to => user.email, :subject => "Your SocialClue Account has Been Cancelled")
  end

  private

  def format_stripe_amount(amount)
    sprintf('$%0.2f', amount.to_f / 100.0).gsub(/(\d)(?=(\d\d\d)+(?!\d))/, "\\1,")
  end

  def format_stripe_date(date)
    Time.at(date).strftime("%m/%d/%Y")
  end
end


