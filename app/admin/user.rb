ActiveAdmin.register User do

  menu priority: 2

  # SCOPES
  scope :has_subscription
  scope :has_active_subscription

  # INDEX PAGE
  index do
    column :id
    column("Email") { |user| link_to user.email, admin_user_path(user) }
    column :created_at
    column :last_sign_in_at
    column :sign_in_count
    column :trial_start
    column :trial_end
    column("Active Subscription") { |user| user.subscription.try(:active) }
    column("Invoices") { |user| user.invoices.size }
    actions
  end

  # INDEX PAGE - SEARCH FILTER
  filter :email, as: :string
  filter :id, as: :string
  filter :created_at, as: :date_range
  filter :trial_start, as: :date_range
  filter :trial_end, as: :date_range
  filter :sign_in_count, as: :numeric

  # SHOW PAGE
  show do |user|
    attributes_table do
      row :email
      row :last_sign_in_at
      row :created_at
      row :updated_at
      row :sign_in_count
      row :trial_start
      row :trial_end
      row("Active Subscription") { |user| user.subscription.try(:active) }
      row("Subscription ID") { |user| user.subscription.try(:id) }
      row("Invoices") { |user| user.invoices.size }
    end
  end

  # UPDATE AND CREATE FORMS
  form do |f|
    f.inputs "Admin Details" do
      f.input :email
      f.input :trial_start
      f.input :trial_end
    end
    f.actions
  end
end
