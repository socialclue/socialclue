ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do

    panel "New Users" do
      div do
        "Users which have been created since #{current_user.email} last signed in at: #{current_user.last_sign_in_at.strftime("%a %d %b %Y - %H:%M")}"
      end

      table_for User.where("created_at > ?", current_user.last_sign_in_at).order('created_at desc') do
        column("Email")           { |user| link_to user.email, admin_user_path(user) }
        column("Created at")      { |user| user.created_at.strftime("%a %d %b %Y - %H:%M") }
        column("Updated at")      { |user| user.updated_at.strftime("%a %d %b %Y - %H:%M") }
      end
    end

    panel "New Subscriptions" do
      div do
        "Subscriptions which have been created since #{current_user.email} last signed in at: #{current_user.last_sign_in_at.strftime("%a %d %b %Y - %H:%M")}"
      end

      table_for Subscription.where("created_at > ?", current_user.last_sign_in_at).order('created_at desc') do
        column("Email")           { |subscription| link_to subscription.user, admin_subscription_path(subscription) }
        column("Created at")      { |subscription| subscription.created_at.strftime("%a %d %b %Y - %H:%M") }
        column("Updated at")      { |subscription| subscription.updated_at.strftime("%a %d %b %Y - %H:%M") }
      end
    end
  end
end
