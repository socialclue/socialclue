ActiveAdmin.register TwitterAccount do

  menu priority: 5

  actions :all, except: [:update, :destroy, :new, :create, :edit]

  index do
    column :name
    column :user
    column :created_at
    column :updated_at
    actions
  end

  # SHOW PAGE

  show do |twitter_account|
    attributes_table do
      row :id
      row :provider
      row :uid
      row :name
      row :user
      row :created_at
      row :updated_at
    end
  end


  # INDEX PAGE - SEARCH FILTER

  filter :name, as: :string
  filter :user, as: :string
  filter :id, as: :string
  filter :created_at, as: :date_range

end
