ActiveAdmin.register Subscription do

  # INDEX PAGE
  menu priority: 3

  index do

    column :id
    column :user
    column :active
    column :active_until
    column :created_at
    column :updated_at
    column :stripe_id
    actions
  end

  # UPDATE AND CREATE FORMS

  form do |f|
    f.inputs "Subscription Details" do
      f.input :user
      f.input :active_until
      f.input :active_from
      f.input :active
    end
    f.actions
  end

end
