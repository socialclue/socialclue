ActiveAdmin.register Invoice do

  menu priority: 4

  actions :all, except: [:update, :destroy, :new, :create, :edit]

  # INDEX PAGE
  index do
    column :id
    column :user
    column :amount
    column :created_at
    column :stripe_reference_id
    column :start_date
    column :end_date

    actions
  end
end
