class Invoice < ActiveRecord::Base
  belongs_to :user

  attr_accessible :user_id, :amount, :stripe_reference_id, :start_date, :end_date

  validates_presence_of :user_id
  validates_presence_of :amount
  validates_presence_of :start_date
  validates_presence_of :end_date
  validates_presence_of :stripe_reference_id
end
