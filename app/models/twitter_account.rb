class TwitterAccount < ActiveRecord::Base
  attr_accessible :name, :provider, :token, :token_secret, :uid, :user

  def twitter
    if self.token && self.token_secret
      twitter_client = Twitter::REST::Client.new do |config|
        config.consumer_key        = ENV["TWITTER_CONSUMER_KEY"]
        config.consumer_secret     = ENV["TWITTER_CONSUMER_SECRET"]
        config.access_token        = self.token
        config.access_token_secret = self.token_secret
     end
    else
      false
    end
  end

  def self.create_with_omniauth(auth)
    create! do |twitter_account|
      twitter_account.provider = auth["provider"]
      twitter_account.uid = auth["uid"]
      twitter_account.name = auth["info"]["name"]
      twitter_account.token = auth['credentials'].token
      twitter_account.token_secret = auth['credentials'].secret
    end
  end

end
