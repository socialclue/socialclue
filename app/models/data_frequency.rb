module DataFrequency

  def top_results
    results.slice(0,5)
  end

  def raw_data
    @freq_object ||= []
  end

  def data_frequency_hash
    @data_frequency_hash ||= Hash.new(0)
  end

  def calculate_frequency
    raw_data.each { |object| data_frequency_hash[object] += 1 }
  end

  def sort_data_frequency_hash
    data_frequency_hash.sort_by {|x,y| y}
  end

  def results
    sort_data_frequency_hash.reverse.each_with_object([]) do |(k, v), array|
      array << k.to_s
    end
  end

end
