class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_one :subscription, dependent: :destroy
  has_many :invoices

  attr_accessible :email, :password, :password_confirmation, :trial_start, :trial_end, :stripe_id

  validate :email, uniqueness: true
  validate :trial_start, presence: true
  validate :trial_end, presence: true

  scope :has_subscription, joins( :subscription )
  scope :has_active_subscription, joins( :subscription ).where('subscriptions.active')

  def twitteraccount user
    TwitterAccount.where(user: user) if TwitterAccount.where(user: user)
  end

  # FOR ACTIVEADMIN
    def display_name
      self.email
    end

    def is_admin?
      self.email && ENV['ADMIN_EMAILS'].to_s.include?(self.email)
    end

  def trial_expired?
    self.trial_end && self.trial_end < Date.today
  end

  def active_subscription?
    if self.trial_end
      if self.trial_expired?
        self.subscription ? subscription_active? : false
      else
        true
      end
    else
      false
    end
  end

  def active_paying_subscription?
    if self.trial_expired?
      self.subscription ? self.subscription.active : false
    end
  end

  private

  def subscription_active?
    if self.subscription.active && self.subscription.active_until
      self.subscription.active_until > Date.today
    end
  end

end
