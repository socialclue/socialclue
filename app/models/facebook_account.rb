class FacebookAccount < ActiveRecord::Base

  attr_accessible :user

  def self.from_omniauth(auth)
    where(auth.slice(:provider, :uid)).first_or_initialize.tap do |facebook_account|
      facebook_account.provider = auth.provider
      facebook_account.uid = auth.uid
      facebook_account.name = auth.info.name
      facebook_account.oauth_token = auth.credentials.token
      facebook_account.oauth_expires_at = Time.at(auth.credentials.expires_at)
      facebook_account.save!
    end
  end

end
