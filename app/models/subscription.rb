class Subscription < ActiveRecord::Base
  belongs_to :user

  attr_accessible :stripe_token, :stripe_email, :default_card, :user_id, :active, :stripe_id, :active_until, :active_from
  attr_accessor :stripe_token, :stripe_email

  before_validation :update_stripe, :unless => Proc.new { |subscription| !subscription.stripe_token.present? }

  validates_presence_of :user_id
  validates_presence_of :default_card
  validates_presence_of :stripe_id

  def update_stripe
    if stripe_id.nil?

      customer = Stripe::Customer.create(
        :email => stripe_email,
        :description => stripe_email,
        :card => stripe_token,
        :plan => "standard_plan"
      )
      self.default_card = customer.default_card
      self.active = true
      self.active_until = (Date.today + 1.month)
    else
      customer = Stripe::Customer.retrieve(stripe_id)
      if stripe_token.present?
        customer.card = stripe_token
      end
      # in case they've changed
      customer.email = stripe_email
      customer.description = stripe_email
      customer.save
      self.default_card = customer.default_card
    end
    self.stripe_id = customer.id
    update_user(customer.id)

    self.stripe_token = nil
  end

  def cancel_stripe
    customer = Stripe::Customer.retrieve(stripe_id)
    customer.cancel_subscription
    customer.save
    self.update_attributes(active: false, active_until: Date.today)
  end

  private

  def update_user stripe_id
    user = User.where("id = ?", self.user_id).first
    user.update_attributes(stripe_id: stripe_id)
  end

end


