class Twitter::TopUserMentions

  include DataFrequency

  def initialize twitter_search_data
    filter_tweet_data twitter_search_data
    calculate_frequency
  end

  private

  def filter_tweet_data data
    data.each do |tweet|
      next unless tweet[:user_mentions]
        raw_data << tweet[:user_mentions][:name]
    end
  end

end
