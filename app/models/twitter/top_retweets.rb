class Twitter::TopRetweets

  def initialize twitter_search_data
    @twitter_search_data = twitter_search_data
  end

  def top_results
    filter_tweet_data.sort_by &:first
  end

  private

  def filter_tweet_data
    @twitter_search_data.each_with_object({}) do |(k, v), most_favorited|
      next unless k[:retweet_count].to_i > 0
        most_favorited[k[:retweet_count]] = [k[:text]] + [k[:username]]
    end
  end

end


