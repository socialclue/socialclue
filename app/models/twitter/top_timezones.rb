class Twitter::TopTimezones

  include DataFrequency

  def initialize twitter_search_data
    filter_tweet_data twitter_search_data
    calculate_frequency
  end

  private

  def filter_tweet_data data
    data.each do |tweet|
      next unless tweet[:time_zone]
        raw_data << tweet[:time_zone]
    end
  end

end


