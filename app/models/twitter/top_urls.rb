class Twitter::TopUrls

  include DataFrequency

  def initialize twitter_search_data
    filter_tweet_data twitter_search_data
    calculate_frequency
  end

  private

  def filter_tweet_data data
    data.each do |tweet|
      next unless tweet[:expanded_url]
        raw_data << tweet[:expanded_url][:expanded_url]
    end
  end

end


