class Twitter::AnalyticsBarchart

  require 'tokenizer'

  STOP_WORDS = [/^had/, /^give/, /^here/, /^come/, /^amp/, /^where/, /^back/, /^next/, /^from/, /^http/, /^23/, /^www/, /^href/, /^\btwitter\b/, /^\bco\b/, /^\ba\b/, /^\busername\b/, /^\brt\b/, /^\bamp\b/, /^\bclass\b/, /^\bhashtag\b/, /^\burl\b/, /^\bvia\b/, /^\bget\b/, /^\blist\b/, /^\bnofollow\b/, /^\bjust\b/, /^\bsearch\b/, /^\bq\b/, /^\btitle\b/, /^\burl\b/, /^\bcom\b/, /^\bus\b/, /^\bwe\b/, /^\bu\b/, /^\brel\b/, /^\bt\b/, /^\btweet\b/, /^\bs\b/, /^\bon\b/, /^\bit\b/, /^\bthe\b/, /^\bthat\b/, /^\bher\b/, /^\bwho\b/, /^\bnot\b/, /^\band\b/, /^\bgoing\b/, /^\bhis\b/, /^\blike\b/, /^\bbe\b/, /^\bto\b/, /^\bof\b/, /^\bfor\b/, /^\bhe\b/, /^\bin\b/, /^\bare\b/, /^\bthis\b/, /^\bwith\b/, /^\bone\b/, /^\bwas\b/, /^\bwhy\b/, /^\bagain\b/, /^\bis\b/, /^\bwill\b/, /^\bcan\b/, /^\bmy\b/, /^\byou\b/, /^\bas\b/, /^\bup\b/, /^\bmore\b/, /^\bfrom\b/, /^\bdoes\b/, /^\bdid\b/, /^\bhow\b/, /^\bcould\b/, /^\bwhen\b/, /^\btheir\b/, /^\bthen\b/, /^\b23\b/, /^\bbut\b/, /^\bno\b/, /^\bonly\b/, /^\bb\b/, /^\bm\b/, /^\bstill\b/, /^\bwhat\b/, /^\bwant\b/, /^\bim\b/, /^\bfollow\b/, /^\bthey\b/, /^\bso\b/, /^\byour\b/, /^\bbc\b/, /^\bor\b/, /^\b5\b/, /^\bat\b/, /^\bi\b/, /^\bd\b/, /^\bc\b/, /^\bme\b/, /^\bif\b/, /^\brather\b/, /^\bthan\b/, /^\babout\b/, /^\bway\b/, /^\bnow\b/, /^\bbecause\b/, /^\bbeing\b/, /^\baway\b/, /^\bhim\b/, /^\bby\b/, /^((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+(:[0-9]+)?|(?:ww‌​w.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?‌​(?:[\w]*))?)/]

  def initialize twitter_search_data
     parse_and_filter_tweets_for_wordfreq_processing twitter_search_data
     find_keywords
  end

  def find_keywords
    config_highscore
    highscore.keywords.top(30).each_with_object([]) do |keyword, keyword_results|
      keyword_results << {x: keyword.text, y: keyword.weight}
    end
  end

  private

  def unimportant? token
    STOP_WORDS.any? do |word|
      word.match token
    end
  end

  def tokeniser tweet
    de_tokenizer = Tokenizer::Tokenizer.new
    de_tokenizer.tokenize(tweet)
  end

  def parse_and_filter_tweets_for_wordfreq_processing data
    @words_for_bar_chart = []
    data.each do |tweet|
      all_tweet_tokens = tokeniser(tweet[:text])
      tweet_words = all_tweet_tokens.each_with_object([]) do |token, arr|
        next if unimportant? token
          arr << token
        end
      @words_for_bar_chart << tweet_words
    end
  end

  def stopwords_for_highscore
    Highscore::Blacklist.load %{win, follow, winner, competition, amp, a's, able, about, above, according, accordingly, across, actually, after, afterwards, again, against, ain’t, all, allow, allows, almost, alone, along, already, also, although, always, am, among, amongst, an, and, another, any, anybody, anyhow, anyone, anything, anyway, anyways, anywhere, apart, appear, appreciate, appropriate, are, aren’t, around, as, aside, ask, asking, associated, at, available, away, awfully, be, became, because, become, becomes, becoming, been, before, beforehand, behind, being, believe, below, beside, besides, best, better, between, beyond, both, brief, but, by, c’mon, c’s, came, can, can’t, cannot, cant, cause, causes, certain, certainly, changes, clearly, co, com, come, comes, concerning, consequently, consider, considering, contain, containing, contains, corresponding, could, couldn’t, course, currently, definitely, described, despite, did, didn’t, different, do, does, doesn’t, doing, don’t, done, down, downwards, during, each, edu, eg, eight, either, else, elsewhere, enough, entirely, especially, et, etc, even, ever, every, everybody, everyone, everything, everywhere, ex, exactly, example, except, far, few, fifth, first, five, followed, following, follows, for, former, formerly, forth, four, from, further, furthermore, get, gets, getting, given, gives, go, goes, going, gone, got, gotten, greetings, had, hadn’t, happens, hardly, has, hasn’t, have, haven’t, having, he, he’s, hello, help, hence, her, here, here’s, hereafter, hereby, herein, hereupon, hers, herself, hi, him, himself, his, hither, hopefully, how, howbeit, however, i’d, i’ll, i’m, i’ve, ie, if, ignored, immediate, in, inasmuch, inc, indeed, indicate, indicated, indicates, inner, insofar, instead, into, inward, is, isn’t, it, it’d, it’ll, it’s, its, itself, just, keep, keeps, kept, know, knows, known, last, lately, later, latter, latterly, least, less, lest, let, let’s, like, liked, likely, little, look, looking, looks, ltd, mainly, many, may, maybe, me, mean, meanwhile, merely, might, more, moreover, most, mostly, much, must, my, myself, name, namely, nd, near, nearly, necessary, need, needs, neither, never, nevertheless, new, next, nine, no, nobody, non, none, noone, nor, normally, not, nothing, novel, now, nowhere, obviously, of, off, often, oh, ok, okay, old, on, once, one, ones, only, onto, or, other, others, otherwise, ought, our, ours, ourselves, out, outside, over, overall, own, particular, particularly, per, perhaps, placed, please, plus, possible, presumably, probably, provides, que, quite, qv, rather, rd, re, really, reasonably, regarding, regardless, regards, relatively, respectively, right, said, same, saw, say, saying, says, second, secondly, see, seeing, seem, seemed, seeming, seems, seen, self, selves, sensible, sent, serious, seriously, seven, several, shall, she, should, shouldn’t, since, six, so, some, somebody, somehow, someone, something, sometime, sometimes, somewhat, somewhere, soon, sorry, specified, specify, specifying, still, sub, such, sup, sure, t’s, take, taken, tell, tends, th, than, thank, thanks, thanx, that, that’s, thats, the, their, theirs, them, themselves, then, thence, there, there’s, thereafter, thereby, therefore, therein, theres, thereupon, these, they, they’d, they’ll, they’re, they’ve, think, third, this, thorough, thoroughly, those, though, three, through, throughout, thru, thus, to, together, too, took, toward, towards, tried, tries, truly, try, trying, twice, two, un, under, unfortunately, unless, unlikely, until, unto, up, upon, us, use, used, useful, uses, using, usually, value, various, very, via, viz, vs, want, wants, was, wasn’t, way, we, we’d, we’ll, we’re, we’ve, welcome, well, went, were, weren’t, what, what’s, whatever, when, whence, whenever, where, where’s, whereafter, whereas, whereby, wherein, whereupon, wherever, whether, which, while, whither, who, who’s, whoever, whole, whom, whose, why, will, willing, wish, with, within, without, won’t, wonder, would, would, wouldn’t, yes, yet, you, you’d, you’ll, you’re, you’ve, your, yours, yourself, yourselves, zero}
  end

  def highscore
    Highscore::Content.new @words_for_bar_chart.flatten.join(' '), stopwords_for_highscore
  end

  def config_highscore
    highscore.configure do
      set :multiplier, 2
      set :upper_case, 3
      set :long_words, 2
      set :long_words_threshold, 15
      set :short_words_threshold, 3      # => default: 2
      set :bonus_multiplier, 2           # => default: 3
      set :vowels, 1                     # => default: 0 = not considered
      set :consonants, 5                 # => default: 0 = not considered
      set :ignore_case, true             # => default: false
      set :word_pattern, /[\w]+[^\s0-9]/ # => default: /\w+/
    end
  end

end
