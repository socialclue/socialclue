class Twitter::TopFavorites

  def initialize twitter_search_data
    @twitter_search_data = twitter_search_data
  end

  def top_results
    filter_tweet_data.sort_by &:first
  end

  private

  def filter_tweet_data
    @twitter_search_data.each_with_object({}) do |(k, v), most_favorited|
      next unless k[:favorite_count].to_i > 0
        most_favorited[k[:favorite_count]] = [k[:text]] + [k[:username]]
    end
  end

end


