class Twitter::TopHashtags

  include DataFrequency

  def initialize twitter_search_data
    filter_tweet_data twitter_search_data
    calculate_frequency
  end

  private

  def filter_tweet_data data
    data.each do |tweet|
      next unless tweet[:hashtag]
        raw_data << tweet[:hashtag][:text]
    end
  end

end
