class Twitter::SentimentAnalysis

 require 'tokenizer'

  STOP_WORDS = %{win, follow, winner, competition, amp, a's, able, about, above, according, accordingly, across, actually, after, afterwards, again, against, ain’t, all, allow, allows, almost, alone, along, already, also, although, always, am, among, amongst, an, and, another, any, anybody, anyhow, anyone, anything, anyway, anyways, anywhere, apart, appear, appreciate, appropriate, are, aren’t, around, as, aside, ask, asking, associated, at, available, away, awfully, be, became, because, become, becomes, becoming, been, before, beforehand, behind, being, believe, below, beside, besides, best, better, between, beyond, both, brief, but, by, c’mon, c’s, came, can, can’t, cannot, cant, cause, causes, certain, certainly, changes, clearly, co, com, come, comes, concerning, consequently, consider, considering, contain, containing, contains, corresponding, could, couldn’t, course, currently, definitely, described, despite, did, didn’t, different, do, does, doesn’t, doing, don’t, done, down, downwards, during, each, edu, eg, eight, either, else, elsewhere, enough, entirely, especially, et, etc, even, ever, every, everybody, everyone, everything, everywhere, ex, exactly, example, except, far, few, fifth, first, five, followed, following, follows, for, former, formerly, forth, four, from, further, furthermore, get, gets, getting, given, gives, go, goes, going, gone, got, gotten, greetings, had, hadn’t, happens, hardly, has, hasn’t, have, haven’t, having, he, he’s, hello, help, hence, her, here, here’s, hereafter, hereby, herein, hereupon, hers, herself, hi, him, himself, his, hither, hopefully, how, howbeit, however, i’d, i’ll, i’m, i’ve, ie, if, ignored, immediate, in, inasmuch, inc, indeed, indicate, indicated, indicates, inner, insofar, instead, into, inward, is, isn’t, it, it’d, it’ll, it’s, its, itself, just, keep, keeps, kept, know, knows, known, last, lately, later, latter, latterly, least, less, lest, let, let’s, like, liked, likely, little, look, looking, looks, ltd, mainly, many, may, maybe, me, mean, meanwhile, merely, might, more, moreover, most, mostly, much, must, my, myself, name, namely, nd, near, nearly, necessary, need, needs, neither, never, nevertheless, new, next, nine, no, nobody, non, none, noone, nor, normally, not, nothing, novel, now, nowhere, obviously, of, off, often, oh, ok, okay, old, on, once, one, ones, only, onto, or, other, others, otherwise, ought, our, ours, ourselves, out, outside, over, overall, own, particular, particularly, per, perhaps, placed, please, plus, possible, presumably, probably, provides, que, quite, qv, rather, rd, re, really, reasonably, regarding, regardless, regards, relatively, respectively, right, said, same, saw, say, saying, says, second, secondly, see, seeing, seem, seemed, seeming, seems, seen, self, selves, sensible, sent, serious, seriously, seven, several, shall, she, should, shouldn’t, since, six, so, some, somebody, somehow, someone, something, sometime, sometimes, somewhat, somewhere, soon, sorry, specified, specify, specifying, still, sub, such, sup, sure, t’s, take, taken, tell, tends, th, than, thank, thanks, thanx, that, that’s, thats, the, their, theirs, them, themselves, then, thence, there, there’s, thereafter, thereby, therefore, therein, theres, thereupon, these, they, they’d, they’ll, they’re, they’ve, think, third, this, thorough, thoroughly, those, though, three, through, throughout, thru, thus, to, together, too, took, toward, towards, tried, tries, truly, try, trying, twice, two, un, under, unfortunately, unless, unlikely, until, unto, up, upon, us, use, used, useful, uses, using, usually, value, various, very, via, viz, vs, want, wants, was, wasn’t, way, we, we’d, we’ll, we’re, we’ve, welcome, well, went, were, weren’t, what, what’s, whatever, when, whence, whenever, where, where’s, whereafter, whereas, whereby, wherein, whereupon, wherever, whether, which, while, whither, who, who’s, whoever, whole, whom, whose, why, will, willing, wish, with, within, without, won’t, wonder, would, would, wouldn’t, yes, yet, you, you’d, you’ll, you’re, you’ve, your, yours, yourself, yourselves, zero}

  def initialize twitter_search_data
    tweets_for_sentiment_processing twitter_search_data
    all_matches
    re_arrange_word_matches_with_original_tweet
    find_tweet_sentiment
  end

  def tweet_body
    find_tweet_sentiment.each_with_object({}) do |(k, v), tweet_info|
      tweet_info[k] = v.round(2)
    end
  end

  def tweet_sentiment_matches
    Hash[find_tweet_sentiment.sort_by {|t| t[0][:created_at] }].each_with_object([]) do |(k, v), array|
      array << v.round(2)
    end
  end

  def tweet_count_negative
    find_tweet_sentiment.values.select{|score| score < 0 }.size
  end

  def tweet_count_positive
    find_tweet_sentiment.values.select{|score| score > 0 }.size
  end

  def percent_positive
    if tweet_count_positive > 0
      ((tweet_count_positive.to_f/tweet_body.size)*100).to_i
    else
      0
    end
  end

  def percent_negative
    if tweet_count_negative > 0
      ((tweet_count_negative.to_f/tweet_body.size)*100).to_i
    else
      0
    end
  end

  private

  def unimportant? token
    STOP_WORDS.include?(token)
  end

  def tokeniser tweet
    de_tokenizer = Tokenizer::Tokenizer.new
    de_tokenizer.tokenize(tweet)
  end

  def tweets_hash
    @tweets_hash ||= {}
  end

  def tweets_for_sentiment_processing data
    data.each do |tweet|
      all_tweet_tokens_bigrams = tokeniser(tweet[:text]).map(&:downcase).each_cons(2)
      tweet_words = all_tweet_tokens_bigrams.each_with_object([]) { |token, arr| arr << token }
      @tweet_time_and_body = {:tweet_id => tweet[:id].to_s, :created_at => tweet[:created_at], :text => tweet[:text], :name => tweet[:name], :username => tweet[:username], :expanded_url => tweet[:expanded_url], :source => tweet[:source], :result_type => tweet[:result_type], :location => tweet[:locaton], :description => tweet[:description], :followers_count => tweet[:followers_count], :friends_count => tweet[:friends_count], :time_zone => tweet[:time_zone], :statuses_count => tweet[:statuses_count], :tweets_count => tweet[:tweets_count], :retweet_count => tweet[:retweet_count], :favorite_count => tweet[:favorite_count], :favorited => tweet[:favorited], :retweeted => tweet[:retweeted]}
      tweets_hash[@tweet_time_and_body] = tweet_words
    end
  end

  def tweeted_at_time
    @tweet_time_and_body
  end

  def process_unigrams_for_lexicon_query
    tweets_hash.map do |(k,v)|
      v.flatten
    end
  end

  def filter_unigrams_for_lexicon_query
    process_unigrams_for_lexicon_query.each_with_object([]) do |token_array, result_array|
      token_array.each do |token|
        next if unimportant? token
          result_array << token
      end
    end
  end

  def process_bigrams_for_lexicon_query
    tweets_hash.map do |(k,v)|
      v.map { |t| t.join(" ") }
    end
  end

  def unigrams_for_lex_query
    filter_unigrams_for_lexicon_query.flatten.uniq
  end

  def bigrams_for_lex_query
    process_bigrams_for_lexicon_query.flatten.uniq
  end

  def all_ngrams
    unigrams_for_lex_query | bigrams_for_lex_query
  end

  def all_matches
    all_unigram_matches = Lexicons.where(ngram: all_ngrams)
  end

  def additional_ngram_attributes
    keyword_lookup = all_matches.each_with_object({}) do |keyword, lookup|
      lookup[keyword.ngram] = keyword
    end
  end

  def tweets_results_hash_unigrams
    tweets_hash.each_with_object({}) do |(k,v), tweets_results_unigrams|
      tweets_results_unigrams[k] =  v.flatten.uniq
    end
  end

  def tweets_results_hash_bigrams
    tweets_hash.each_with_object({}) do |(k,v), tweets_results_bigrams|
      tweets_results_bigrams[k] = v.map { |t| t.join(" ") }
    end
  end

  def tweets_results_hash_merged
    tweets_results_hash_unigrams.merge(tweets_results_hash_bigrams){|key,oldval,newval| oldval + newval }
  end

  def ngram_data
    @ngram_data ||= additional_ngram_attributes
  end

  def re_arrange_word_matches_with_original_tweet
    tweets_results_hash_merged.each_with_object({}) do |(tweeted_at_time, words), result_hash|
      tweet_result = words.each_with_object([]) do |word, tweet_array|
        match = ngram_data[word]
        tweet_array << {ngram: word, score: match.score} if match
      end
      result_hash[tweeted_at_time] = tweet_result unless tweet_result.empty?
    end
  end

  def find_tweet_sentiment
    @sentiment_results ||= re_arrange_word_matches_with_original_tweet.each_with_object({}) do |(tweeted_at_time, array_tweet), avg_tweet_sentiment|
      tweet_sentiment = array_tweet.map { |sentiment| sentiment[:score].to_f }
      tweet_avg = tweet_sentiment.reduce(:+)
      avg_tweet_sentiment[tweeted_at_time] = (tweet_avg/array_tweet.count)
    end
  end
end
