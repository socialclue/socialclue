class Facebook::TypeFrequency

  include DataFrequency

  def initialize graph_feed_data
    filter_feed_data graph_feed_data
    calculate_frequency
  end

  private

  def filter_feed_data data
    data.each do |feed_object|
      next unless feed_object[:type]
        raw_data << feed_object[:type]
    end
  end

end
