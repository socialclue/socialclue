class Facebook::MostShared

  def initialize nodes
    @sentiment_analysis = Facebook::SentimentAnalysis.new(nodes).message_sentiment
  end

  def top_results
    filter_feed_data.sort_by &:first
  end

  private

  def filter_feed_data
    @sentiment_analysis.each_with_object({}) do |(k, v), most_shared|
      next unless k[:shares].to_i > 0
        most_shared[k[:shares]] = k
    end
  end
end
