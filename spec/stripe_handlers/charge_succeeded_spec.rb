require "spec_helper"

describe ChargeSucceeded do

  describe "#call" do

    before { StripeMock.start }
    after { StripeMock.stop }

    let(:user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, :subscription_expired, user_id: user.id)
      user
    }

    it "updates the current user's subscription" do
      event = StripeMock.mock_webhook_event('charge.succeeded', {
        :customer => user.stripe_id,
        :email => user.email
      })

      ChargeSucceeded.new.call(event)

      expect(user.subscription.active_until).to eq(Date.today + 1.month)
    end

    it "creates a new invoice for the user" do
      event = StripeMock.mock_webhook_event('charge.succeeded', {
        :customer => user.stripe_id,
        :email => user.email
      })

      expect {
        ChargeSucceeded.new.call(event)
      }.to change{ Invoice.count }.by(1)
    end
  end
end


