require "spec_helper"

describe PaymentFailed do

  describe "#call" do

    before { StripeMock.start }
    after { StripeMock.stop }

    let(:user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      user
    }

    it "updates the current user's subscription" do
      event = StripeMock.mock_webhook_event('invoice.payment_failed', {
        :customer => user.stripe_id,
        :email => user.email,
        :amount => "10"
      })

      PaymentFailed.new.call(event)

      expect(user.subscription.active_until).to eq(Date.today + 3.days )
    end
  end
end


