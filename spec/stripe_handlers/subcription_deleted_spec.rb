require "spec_helper"

describe SubscriptionDeleted do

  describe "#call" do

    before { StripeMock.start }
    after { StripeMock.stop }

    let(:user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      user
    }

    it "updates the current user's subscription" do
      event = StripeMock.mock_webhook_event('customer.subscription.deleted', {
        :customer => user.stripe_id,
        :email => user.email
      })

      SubscriptionDeleted.new.call(event)

      expect(user.subscription.active_until).to eq(Date.today)
      expect(user.subscription.active).to eq(false)
    end
  end
end
