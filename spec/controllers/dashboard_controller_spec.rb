require 'spec_helper'

describe DashboardController do

  context "user is not signed in" do
    it "redirects to sign in page" do
      get :index
      expect(response).to redirect_to('/users/sign_in')
    end
  end

  context "user has an active account" do
    let(:current_user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      user
    }

    before { sign_in current_user }

    it "returns http success" do
      get :index
      response.should be_success
    end

    it "renders the index template" do
      get :index
      expect(response).to render_template("index")
    end
  end

  context "user has an inactive account" do
    let(:current_user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, :subscription_expired, user_id: user.id)
      user
    }

    before { sign_in current_user }

    it "redirect_to the new subscriptions template" do
      get :index
      expect(response).to redirect_to new_subscription_path
    end
  end
end



