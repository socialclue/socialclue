require 'spec_helper'

describe HomeController do

  context "user visits home page" do
    it "renders the home page" do
      get :index
      expect(response).to render_template("index")
    end

    it "returns http success" do
      get :index
      response.should be_success
    end
  end
end
