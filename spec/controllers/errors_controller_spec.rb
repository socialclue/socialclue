require 'spec_helper'

describe ErrorsController do

  before (:each) { controller.stub(:current_user).and_return(FactoryGirl.create(:user)) }

  context "404 error" do
    it "renders the corrent error page" do
      get :not_found
      expect(response).to render_template("not_found")
    end
  end

  context "500 error" do
    it "renders the corrent error page" do
      get :server_error
      expect(response).to render_template("server_error")
    end
  end
end
