require 'spec_helper'

describe Twitter::PhotosController do

  before (:each) do
    controller.stub(:current_user).and_return(FactoryGirl.create(:user))
  end

  context "user is not signed in" do
    it "redirects to sign in page" do
      get :search
      expect(response).to redirect_to('/users/sign_in')
    end
  end

  context "User is not signed in to twitter" do
    describe "GET search" do
      before { controller.stub(:authenticate_user!).and_return(true) }
      before { get :search }

      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/twitter?use_authorize=true"
      end
    end
  end

  context "User is signed in to twitter" do
    describe "GET search" do
      before (:each) do
        controller.stub(:authenticate_user!).and_return(true)
        request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:twitter]
        controller.stub(:current_twitter_account).and_return(true)
      end

      context "no search params given" do
        before { controller.stub(:blank?).and_return(false) }
        before { get :search }

        it "renders search template" do
          expect(response).to render_template("search")
        end
        it "returns http success" do
          response.should be_success
        end
      end

      context "search params are present" do
        before { controller.stub(:blank?).and_return(true) }
        before { get :search }

        it "renders search template" do
          expect(response).to render_template("search")
        end
        it "returns http success" do
          response.should be_success
        end
      end
    end
  end

end
