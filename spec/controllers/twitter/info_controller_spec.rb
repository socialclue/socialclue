require 'spec_helper'

describe Twitter::InfoController do

  before (:each) { controller.stub(:current_user).and_return(FactoryGirl.create(:user)) }

  context "user is not signed in" do
    it "redirects to sign in page" do
      get :about
      expect(response).to redirect_to('/users/sign_in')
    end
  end

  context "User is not signed in to twitter" do
    before { controller.stub(:authenticate_user!).and_return(true) }

    it "redirects to the user auth route" do
      get :about
      expect(response).to redirect_to "/auth/twitter?use_authorize=true"
    end
  end

  context "User is signed in to twitter" do
    describe "GET info" do
      before (:each) do
        controller.stub(:authenticate_user!).and_return(true)
        request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:twitter]
        controller.stub(:current_twitter_account).and_return(true)

        controller.stub(:blank?).and_return(false)
      end

      it "renders info template" do
        get :about
        expect(response).to render_template("about")
      end

      it "returns http success" do
        get :about
        response.should be_success
      end
    end
  end
end
