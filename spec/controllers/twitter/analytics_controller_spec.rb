require 'spec_helper'

describe Twitter::AnalyticsController do

  before (:each) do
    controller.stub(:current_user).and_return(FactoryGirl.create(:user))
  end

  context "user is not signed in" do
    it "redirects to sign in page" do
      get :sentiment
      expect(response).to redirect_to('/users/sign_in')
    end
  end

  context "User is not signed in to twitter" do
    before (:each) do
      controller.stub(:authenticate_user!).and_return(true)
    end

    describe "GET start" do
      before { get :start }

      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/twitter?use_authorize=true"
      end
    end

    describe "GET sentiment" do
      before { get :sentiment }

      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/twitter?use_authorize=true"
      end
    end

    describe "GET stats" do
      before { get :stats }

      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/twitter?use_authorize=true"
      end
    end

    describe "GET keywords" do
      before { get :keywords }

      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/twitter?use_authorize=true"
      end
    end
  end

  context "User is signed in to twitter" do
    before (:each) do
      controller.stub(:authenticate_user!).and_return(true)
      request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:twitter]
      controller.stub(:current_twitter_account).and_return(true)
    end

    describe "GET start" do
      before { get :start }

      it "returns http success" do
        response.should be_success
      end
      it "renders sentiment template" do
        expect(response).to render_template("start")
      end
    end

    describe "GET sentiment" do
      before { get :sentiment }

      it "returns http success" do
        response.should be_success
      end
      it "renders sentiment template" do
        expect(response).to render_template("sentiment")
      end
    end

    describe "GET stats" do
      before { get :stats }

      it "returns http success" do
        response.should be_success
      end
      it "renders stats template" do
        expect(response).to render_template("stats")
      end
    end

    describe "GET keywords" do
      before { get :keywords }

      it "returns http success" do
        response.should be_success
      end
      it "renders keywords template" do
        expect(response).to render_template("keywords")
      end
    end
  end
end
