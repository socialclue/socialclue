require 'spec_helper'

describe Facebook::AnalyticsController do

  include Devise::TestHelpers

  before (:each) do
    controller.stub(:current_user).and_return(FactoryGirl.create(:user))
  end

  context "user is not signed in" do
    it "redirects to sign in page" do
      get :start
      expect(response).to redirect_to('/users/sign_in')
    end
  end

  context "User is not signed in to facebook" do
    before (:each) do
      controller.stub(:authenticate_user!).and_return(true)
    end

    describe "GET start" do
      before { get :start }
      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/facebook"
      end
    end

    describe "GET search" do
      before { get :search }
      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/facebook"
      end
    end

    describe "GET feed" do
      before { get :feed }
      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/facebook"
      end
    end

    describe "GET stats" do
      before { get :stats }
      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/facebook"
      end
    end

    describe "GET keywords" do
      before { get :keywords }
      it "redirects to the user auth route" do
        expect(response).to redirect_to "/auth/facebook"
      end
    end
  end

  context "User is signed in to facebook" do
    before  (:each) do
      controller.stub(:authenticate_user!).and_return(true)
      request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
      controller.stub(:current_facebook_account).and_return(true)
      controller.stub(:oauth_token).and_return(12334343)
    end

    describe "GET start" do
      before { get :start }

      it "returns http success" do
        response.should be_success
      end

      it "renders sentiment template" do
        expect(response).to render_template("start")
      end
    end

    describe "GET feed" do
      before { stub_request(:get, "https://graph.facebook.com/feed?access_token=12334343").
                with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.8.8'}).
                to_return(:status => 200, :body => "{}", :headers => {})
              }
      before { get :feed }

      it "returns http success" do
        response.should be_success
      end
      it "renders stats template" do
        expect(response).to render_template("feed")
      end
    end

    describe "GET search" do
      before { stub_request(:get, "https://graph.facebook.com/search?access_token=12334343&type").
                with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.8.8'}).
                to_return(:status => 200, :body => "{}", :headers => {})
              }
      before { get :search }

      it "returns http success" do
        response.should be_success
      end
      it "renders stats template" do
        expect(response).to render_template("search")
      end
    end

    describe "GET stats" do
      before { stub_request(:get, "https://graph.facebook.com/feed?access_token=12334343").
                with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.8.8'}).
                to_return(:status => 200, :body => "{}", :headers => {})
              }

      before { get :stats }

      it "returns http success" do
        response.should be_success
      end
      it "renders stats template" do
        expect(response).to render_template("stats")
      end
    end

    describe "GET keywords" do
      before { stub_request(:get, "https://graph.facebook.com/feed?access_token=12334343").
                with(:headers => {'Accept'=>'*/*', 'Accept-Encoding'=>'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent'=>'Faraday v0.8.8'}).
                to_return(:status => 200, :body => "{}", :headers => {})
              }
      before { get :keywords }

      it "returns http success" do
        response.should be_success
      end
      it "renders keywords template" do
        expect(response).to render_template("keywords")
      end
    end
  end
end
