require 'spec_helper'

describe AccountsController do

  before (:each) { controller.stub(:current_user).and_return(FactoryGirl.create(:user)) }

  context "user is not signed in" do
    it "redirects to sign in page" do
      get :show
      expect(response).to redirect_to('/users/sign_in')
    end
  end

  context "User is signed in" do
    before { controller.stub(:authenticate_user!).and_return(true) }

    it "returns http success" do
      get :show
      response.should be_success
    end

    it "renders the corrent template" do
      get :show
      expect(response).to render_template("show")
    end
  end
end
