require 'spec_helper'

describe SessionsController do
  include Devise::TestHelpers

  describe "#create_twitter" do
    before (:each) do
      controller.stub(:current_user).and_return(FactoryGirl.create(:user))
      controller.stub(:authenticate_user!).and_return(true)
      request.env["devise.mapping"] = Devise.mappings[:user]
      request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:twitter]
    end

    it "successfully creates a twitter account" do
      expect {
        post :create_twitter, provider: :twitter
      }.to change{ TwitterAccount.count }.by(1)
    end

    it "successfully creates a twitter session" do
      session[:twitter_account_id].should be_nil
      post :create_twitter, provider: :twitter
      session[:twitter_account_id].should_not be_nil
    end

    it "redirects the user to the root url" do
      post :create_twitter, provider: :twitter
      response.should redirect_to start_twitter_analytics_path
    end
  end

  describe "#create_facebook" do
    before (:each) do
      controller.stub(:current_user).and_return(FactoryGirl.create(:user))
      controller.stub(:authenticate_user!).and_return(true)
      request.env["devise.mapping"] = Devise.mappings[:user]
      FacebookAccount.stub(:from_omniauth).and_return(FactoryGirl.create(:facebook_account))
      request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
      FacebookAccount.stub(:find_by_provider_and_uid).and_return(FactoryGirl.create(:facebook_account))
    end

    it "successfully creates a facebook session" do
      session[:facebook_account_id].should be_nil
      post :create_facebook, provider: :facebook
      session[:facebook_account_id].should_not be_nil
    end

    it "redirects the user to the root url" do
      post :create_facebook, provider: :facebook
      response.should redirect_to start_facebook_analytics_path
    end
  end

  describe "#destroy" do
    before do
      controller.stub(:current_user).and_return(FactoryGirl.create(:user))
      controller.stub(:authenticate_user!).and_return(true)
      request.env["devise.mapping"] = Devise.mappings[:user]

      request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:twitter]
      post :create_twitter, provider: :twitter

      FacebookAccount.stub(:from_omniauth).and_return(FactoryGirl.create(:facebook_account))
      controller.stub(:current_user).and_return(FactoryGirl.create(:user))
      request.env["omniauth.auth"] = OmniAuth.config.mock_auth[:facebook]
      FacebookAccount.stub(:find_by_provider_and_uid).and_return(FactoryGirl.create(:facebook_account))
      post :create_facebook, provider: :facebook

    end

    it "clears the current twitter session" do
      session[:twitter_account_id].should_not be_nil
      delete :destroy
      session[:twitter_account_id].should be_nil
    end

    it "clears the current facebook session" do
      session[:facebook_account_id].should_not be_nil
      delete :destroy
      session[:facebook_account_id].should be_nil
    end

    it "redirects to the home page" do
      delete :destroy
      response.should redirect_to root_url
    end
  end
end
