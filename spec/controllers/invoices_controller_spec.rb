require 'spec_helper'

describe InvoicesController do

  before (:each) { controller.stub(:current_user).and_return(FactoryGirl.create(:user)) }

  context "user is not signed in" do
    it "redirects to sign in page" do
      get :index
      expect(response).to redirect_to('/users/sign_in')
    end
  end

  context "User is signed in" do
    before { controller.stub(:authenticate_user!).and_return(true) }

    it "returns http success" do
      get :index
      response.should be_success
    end

    it "renders the corrent template" do
      get :index
      expect(response).to render_template("index")
    end
  end
end
