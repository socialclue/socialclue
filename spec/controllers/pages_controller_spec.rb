require 'spec_helper'

describe PagesController do

  context "user visits about page" do
    it "renders the home page" do
      get :about
      expect(response).to render_template("about")
    end

    it "returns http success" do
      get :about
      response.should be_success
    end
  end

  context "user visits 'learn more' page" do
    it "renders the 'learn more' page" do
      get :learn_more
      expect(response).to render_template("learn_more")
    end

    it "returns http success" do
      get :learn_more
      response.should be_success
    end
  end

  context "user visits privacy_policy page" do
    it "renders the privacy_policy page" do
      get :privacy_policy
      expect(response).to render_template("privacy_policy")
    end

    it "returns http success" do
      get :privacy_policy
      response.should be_success
    end
  end

  context "user visits pricing page" do
    it "renders the pricing page" do
      get :pricing
      expect(response).to render_template("pricing")
    end

    it "returns http success" do
      get :pricing
      response.should be_success
    end
  end

  context "user visits 'terms of service' page" do
    it "renders the 'terms of service' page" do
      get :terms_of_service
      expect(response).to render_template("terms_of_service")
    end

    it "returns http success" do
      get :terms_of_service
      response.should be_success
    end
  end
end
