require 'spec_helper'

describe SubscriptionsController do
  include Devise::TestHelpers

  describe "GET new" do
    context "user does not have an active account" do
      let(:current_user) { FactoryGirl.create(:user, :trial_has_expired) }
      before { sign_in current_user }
      before { get :new }

      it "returns http success" do
        response.should be_success
      end
    end

    context "user has an active account" do
      let(:current_user) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, user_id: user.id)
        user
      }
      before { sign_in current_user }
      before { get :new }

      it "redirects to the dashboard" do
        expect(response).to redirect_to dashboard_path
      end
    end
  end

  describe "POST create" do
    let(:current_user) { FactoryGirl.create(:user, :trial_has_expired) }
    before do
      sign_in current_user
      successful_stripe_response = StripeHelper::Response.new("success")
      Stripe::Customer.stub(:create).and_return(successful_stripe_response)
    end

    it "creates a new subscription" do
      expect {
        post :create, stripeEmail: 'test21@example.com', stripeToken: 'test21token'
      }.to change{ Subscription.count }.by(1)
      expect(current_user.subscription.active).to eq true
    end
  end

  describe "GET 'edit'" do
    let(:current_user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      user
    }
    before { sign_in current_user }
    before { get :edit }

    it "returns http success" do
      response.should be_success
    end
  end

  describe "PATCH 'update'" do
    let(:current_user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      user
    }
    before { sign_in current_user }

    it "updates subscription" do
      expect {
        put :update, stripeEmail: 'test21@example.com', stripeToken: 'test21token'
      }.to change{ Subscription.count }.by(0)
    end
  end

  describe "PATCH 'cancel'" do
    before { StripeMock.start }
    after { StripeMock.stop }

    let(:user) { FactoryGirl.create(:user, :trial_has_expired) }
    let(:subscription) { FactoryGirl.build(:subscription, :skeleton_subscription, user_id: user.id) }
    let(:customer) { Stripe::Customer.create({ email: user.email, card: 'void_card_token' }) }

    before do
      subscription.default_card = customer.default_card
      subscription.active = true
      subscription.active_until = (Date.today + 1.month)
      subscription.stripe_id = customer.id
      subscription.save
    end

    before { sign_in user }

    it "cancels users subscription" do
      # expect(user.subscription.active).to eq true
      # put :cancel
      # expect(user.subscription.active).to eq false
    end
  end
end





