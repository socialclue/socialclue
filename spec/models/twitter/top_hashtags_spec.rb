require "spec_helper"

describe Twitter::TopHashtags do

  describe "#top_results" do

   search_results = [
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>
          {:text=>"WWATourWembley6", :indices=>[70, 85]},
        :user_mentions=>
          {:screen_name=>"onedirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>
          {:text=>"WWATourWembley5", :indices=>[70, 85]},
        :user_mentions=>
          {:screen_name=>"twodirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>
          {:text=>"WWATourWembley4", :indices=>[70, 85]},
        :user_mentions=>
          {:screen_name=>"threeodirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>
          {:text=>"WWATourWembley3", :indices=>[70, 85]},
        :user_mentions=>
          {:screen_name=>"fourdirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>
          {:text=>"WWATourWembley2", :indices=>[70, 85]},
        :user_mentions=>
          {:screen_name=>"fivedirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :hashtag=>
          {:text=>"WWATourWembley1", :indices=>[70, 85]},
        :user_mentions=>
          {:screen_name=>"sixdirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      }
    ]

    let(:top_hashtags) { Twitter::TopHashtags.new(search_results) }

    it "returns the correct number of results" do
      expect(top_hashtags.top_results.count).to eq 5
    end

    it "returns the correct top five results" do
      expect(top_hashtags.top_results).to eq(["WWATourWembley1", "WWATourWembley2", "WWATourWembley3", "WWATourWembley4", "WWATourWembley5"])
    end

  end

end
