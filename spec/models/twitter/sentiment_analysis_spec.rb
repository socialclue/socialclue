require "spec_helper"

describe Twitter::SentimentAnalysis do

  # let(:search_results) { File.new(Rails.root.join("spec", "fixtures", "twitter_search_results.json")) }

  search_results = [
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>
          {:url=>"http://t.co/czam3HNrkP5",
           :expanded_url=>"http://www.mirror.co.uk/news/uk-news/outrage-anti-homeless-spikes-spotte5",
           :display_url=>"mirror.co.uk/news/uk-news/o…5",
           :indices=>[93, 115]},
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"onedirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>
          {:url=>"http://t.co/czam3HNrkP3",
           :expanded_url=>"http://www.mirror.co.uk/news/uk-news/outrage-anti-homeless-spikes-spotte3",
           :display_url=>"mirror.co.uk/news/uk-news/o…3",
           :indices=>[93, 115]},
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"twodirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>
          {:url=>"http://t.co/czam3HNrkP2",
           :expanded_url=>"http://www.mirror.co.uk/news/uk-news/outrage-anti-homeless-spikes-spotte2",
           :display_url=>"mirror.co.uk/news/uk-news/o…2",
           :indices=>[93, 115]},
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"threeodirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>
          {:url=>"http://t.co/czam3HNrkP1",
           :expanded_url=>"http://www.mirror.co.uk/news/uk-news/outrage-anti-homeless-spikes-spotte1",
           :display_url=>"mirror.co.uk/news/uk-news/o…1",
           :indices=>[93, 115]},
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"fourdirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>
          {:url=>"http://t.co/czam3HNrkP",
           :expanded_url=>"http://www.mirror.co.uk/news/uk-news/outrage-anti-homeless-spikes-spotte",
           :display_url=>"mirror.co.uk/news/uk-news/o…",
           :indices=>[93, 115]},
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"fivedirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>
          {:url=>"http://t.co/exrU6AeLCz",
          :expanded_url=>"http://instagram.com/p/o6uoT8wuiH/",
          :display_url=>"instagram.com/p/o6uoT8wuiH/",
          :indices=>[73, 95]},
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"sixdirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      }
    ]

  describe "#tweet_body" do
    let(:sentiment_analysis) { Twitter::SentimentAnalysis.new(search_results) }

    # it "returns a body of tweets" do
    #   expect(sentiment_analysis.tweet_body).not_to be_empty
    # end
  end

  describe "#tweet_sentiment_matches" do
    context "when no matches are found" do
      let(:sentiment_analysis) { Twitter::SentimentAnalysis.new(search_results) }

      it "returns an empty collection" do
        expect(sentiment_analysis.tweet_sentiment_matches).to be_empty
      end
    end

    context "when matches are found" do
      let(:sentiment_analysis) { Twitter::SentimentAnalysis.new(search_results) }

      # it "returns a collection of tweets" do
      #   expect(sentiment_analysis.tweet_sentiment_matches).not_to be_empty
      # end
    end
  end

  describe "#percent_positive" do
    let(:sentiment_analysis) { Twitter::SentimentAnalysis.new(search_results) }

    # it "returns the correct percentage of positive tweets" do
    #   expect(sentiment_analysis.percent_positive).to eq(90)
    # end
  end

  describe "#percent_negative" do
    let(:sentiment_analysis) { Twitter::SentimentAnalysis.new(search_results) }

    # it "returns the correct percentage of positive tweets" do
    #   expect(sentiment_analysis.percent_negative).to eq(10)
    # end
  end

end
