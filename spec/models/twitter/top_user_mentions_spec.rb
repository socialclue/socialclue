require "spec_helper"

describe Twitter::TopUserMentions do

  describe "#top_results" do

   search_results = [
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"onedirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"twodirection",
           :name=>"Two Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"threeodirection",
           :name=>"Three Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"fourdirection",
           :name=>"Four Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"fivedirection",
           :name=>"Five Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"sixdirection",
           :name=>"Six Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      }
    ]

    let(:top_user_mentions) { Twitter::TopUserMentions.new(search_results) }

    it "returns the correct number of results" do
      expect(top_user_mentions.top_results.count).to eq 5
    end

    it "returns the correct results" do
      expect(top_user_mentions.top_results).to eq (["Six Direction", "Five Direction", "Four Direction", "Three Direction", "Two Direction"])
    end

  end

end
