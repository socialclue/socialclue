require "spec_helper"

describe Twitter::TopSources do

  describe "#top_results" do

   search_results = [
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"onedirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone1</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"twodirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone2</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"threeodirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone3</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"fourdirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone4</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"fivedirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone5</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      },
      {
        :text=>"Absolutely perfect day in London! Suns out! Gona be a cracker at wembley tonight ! Yeaaaaahhhh!",
        :name=>"Niall Horan",
        :username=>"NiallOfficial",
        :expanded_url=>nil,
        :hashtag=>nil,
        :user_mentions=>
          {:screen_name=>"sixdirection",
           :name=>"One Direction",
           :id=>209708391,
           :id_str=>"209708391",
           :indices=>[8, 21]},
        :source=>"<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone6</a>",
        :result_type=>"popular",
        :location=>"Mullingar,Westmeath,Ireland",
        :description=>"i like climbing trees ! NOW! KETCHUP",
        :followers_count=>18630753,
        :friends_count=>4942,
        :time_zone=>"London",
        :statuses_count=>9219,
        :tweets_count=>9219,
        :retweet_count=>60461,
        :favorite_count=>99009,
        :favorited=>false,
        :retweeted=>false
      }
    ]

    let(:top_sources) { Twitter::TopSources.new(search_results) }

    it "returns the correct number of results" do
      expect(top_sources.top_results.count).to eq 5
    end

    it "returns the most frequent words" do
      expect(top_sources.top_results).to eq(["<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone6</a>", "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone5</a>", "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone4</a>", "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone3</a>", "<a href=\"http://twitter.com/download/iphone\" rel=\"nofollow\">Twitter for iPhone2</a>"])
    end

  end

end
