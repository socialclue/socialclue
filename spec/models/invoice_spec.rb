require "spec_helper"

describe Invoice do

  it "has a valid factory" do
    FactoryGirl.create(:invoice).should be_valid
  end

  describe "validations" do
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:stripe_reference_id) }
    it { should validate_presence_of(:amount) }
    it { should validate_presence_of(:start_date) }
    it { should validate_presence_of(:end_date) }
  end

end
