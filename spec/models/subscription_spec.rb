require "spec_helper"

describe Subscription do

  it "has a valid factory" do
    FactoryGirl.create(:subscription, user_id: 1).should be_valid
  end

  describe "validations" do
    it { should validate_presence_of(:user_id) }
    it { should validate_presence_of(:default_card) }
    it { should validate_presence_of(:stripe_id) }
  end

  describe "#update_stripe" do
    before { StripeMock.start }
    after { StripeMock.stop }

    let(:user) { FactoryGirl.create(:user, :trial_has_expired) }
    let(:subscription) { FactoryGirl.build(:subscription, :skeleton_subscription, user_id: user.id) }
    let(:customer) { Stripe::Customer.create({ email: user.email, card: 'void_card_token' }) }

    before do
      subscription.default_card = customer.default_card
      subscription.active = true
      subscription.active_until = (Date.today + 1.month)
      subscription.stripe_id = customer.id
      subscription.save
    end

    context "stripe_id is nil" do
      it "creates a new stripe subscription" do
        expect(user.subscription.default_card).to eq(customer.default_card)
        expect(user.subscription.stripe_id).to eq(customer.id)
        expect(user.subscription.active).to eq(true)
      end
    end

    context "stripe id is not nil" do
      let(:updated_customer) { Stripe::Customer.retrieve(user.subscription.stripe_id) }

      before do
        updated_customer.card = 'new_void_card_token'
        updated_customer.email = user.email
        updated_customer.description = user.email
        updated_customer.save
        user.subscription.default_card = updated_customer.default_card
        user.subscription.stripe_id = updated_customer.id
        user.subscription.save
      end

      it "updates current user's stripe subscription" do
        expect(user.subscription.default_card).to eq(updated_customer.default_card)
        expect(user.subscription.stripe_id).to eq(updated_customer.id)
        expect(user.subscription.active).to eq(true)
      end
    end
  end

  describe "#cancel_stripe" do

  end
end
