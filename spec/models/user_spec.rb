require 'spec_helper'

describe User do

  it "has a valid factory" do
    FactoryGirl.create(:user).should be_valid
  end

  context "validations" do
    it { should validate_uniqueness_of(:email) }
  end

  context "User provides correct information" do
    let(:user) { FactoryGirl.create(:user) }

    it "should be valid" do
      expect(user.valid?).to eq true
    end
  end

  context "User provides incorrect information" do
    before(:each) { user.valid? }

    context "does not provide email" do
      let(:user) { FactoryGirl.build(:user, :has_empty_email) }

      it "raises a Record Invlaid error" do
        expect{ user.save! }.to raise_error  ActiveRecord::RecordInvalid
      end
    end

    context "does not provide password" do
      let(:user) { FactoryGirl.build(:user, :has_empty_password) }

      it "raises a Record Invalid error" do
        expect{ user.save! }.to raise_error ActiveRecord::RecordInvalid
      end
    end

    context "does not provide password confirmation" do
      let(:user) { FactoryGirl.build(:user, :has_empty_password_confirmation) }

      it "raises a Record Invalid error" do
        expect{ user.save! }.to raise_error ActiveRecord::RecordInvalid
      end
    end

    context "does not provide the same password and password_confirmation" do
      let(:user) { FactoryGirl.build(:user, :has_wrong_password_confirmation) }

      it "raises a Record Invalid error" do
        expect{ user.save! }.to raise_error ActiveRecord::RecordInvalid
      end
    end
  end

  describe "#trial_expired?" do
    context "trial has not expired" do
      let(:user) { FactoryGirl.build(:user) }

      it "returns false" do
        expect(user.trial_expired?).to eq(false)
      end
    end

    context "trial has expired" do
      let(:expired_user) { FactoryGirl.build(:user, :trial_has_expired) }

      it "returns true" do
        expect(expired_user.trial_expired?).to eq(true)
      end
    end
  end

  describe "#active_subscription?" do

    context "subscription is active" do
      let(:user_with_active_subscription) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, user_id: user.id)
        user
      }

      it "returns true" do
        expect(user_with_active_subscription.active_subscription?).to eq(true)
      end
    end

    context "subscription is inactive" do
      let(:user_with_inactive_subscription) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, :subscription_expired, user_id: user.id)
        user
      }

      it "does not return true" do
        expect(user_with_inactive_subscription.active_subscription?).not_to eq(true)
      end
    end

    context "user does not have a paid subscription" do
      context "trial has not expired" do
        let(:user) { FactoryGirl.build(:user) }

        it "returns true" do
          expect(user.active_subscription?).to eq(true)
        end
      end

      context "trial has expired" do
        let(:expired_user) { FactoryGirl.build(:user, :trial_has_expired) }

        it "returns false" do
          expect(expired_user.active_subscription?).to eq(false)
        end
      end
    end
  end

  describe "#active_paying_subscription?" do
    context "subscription is active" do
      let(:user_with_active_subscription) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, user_id: user.id)
        user
      }

      it "returns true" do
        expect(user_with_active_subscription.active_paying_subscription?).to eq(true)
      end
    end

    context "subscription is inactive" do
      let(:user_with_inactive_subscription) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, :subscription_expired, user_id: user.id)
        user
      }

      it "returns false" do
        expect(user_with_inactive_subscription.active_paying_subscription?).to eq(false)
      end
    end

    context "user does not have a paid subscription" do
      context "trial has not expired" do
        let(:user) { FactoryGirl.build(:user) }

        it "does not return true" do
          expect(user.active_paying_subscription?).not_to eq(true)
        end
      end

      context "trial has expired" do
        let(:expired_user) { FactoryGirl.build(:user, :trial_has_expired) }

        it "does not return true" do
          expect(expired_user.active_paying_subscription?).not_to eq(true)
        end
      end
    end
  end
end

