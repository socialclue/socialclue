require "factory_girl"

FactoryGirl.define do

  factory :facebook_account do
    name "test"
    oauth_token 12345
    uid 123456
    user "test@test.com"
    oauth_expires_at "Sun, 06 Jul 2014"
  end

end
