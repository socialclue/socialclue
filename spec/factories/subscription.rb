require "factory_girl"

FactoryGirl.define do

  factory :subscription do
    default_card "card_14PevW2eZvKYlo2Cq1KkhJ3c"
    stripe_id "cus_4YmU4hxOFTbkAv"
    active_until Date.today + 1.month
    active true
    active_from Date.today
  end

  trait :subscription_expired do
    active false
    active_from Date.today - 1.month
    active_until Date.today - 1.week
  end

  trait :skeleton_subscription do
    default_card ""
    stripe_id ""
    active_until Date.today - 1.week
    active false
    active_from Date.today - 1.month
  end
end


