require "factory_girl"

FactoryGirl.define do

  factory :invoice do
    user_id 1
    amount "1000"
    stripe_reference_id "example_charge_reference"
    start_date Date.today
    end_date Date.today + 1.month
  end

end
