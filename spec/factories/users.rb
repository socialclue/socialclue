require "factory_girl"

FactoryGirl.define do

  sequence :email do |n|
    "test#{n}@example.com"
  end

  factory :user do
    email
    password "12345678"
    password_confirmation "12345678"
    trial_start Date.today
    trial_end Date.today + 14.days
    stripe_id "cus_my_custom_value"
  end

  trait :has_empty_email do
    email ""
  end

  trait :has_empty_password do
    password ""
  end

  trait :has_empty_password_confirmation do
    password "12345678"
    password_confirmation ""
  end

  trait :has_wrong_password_confirmation do
    password_confirmation "testers"
  end

  trait :trial_has_expired do
    trial_start Date.today - 1.month
    trial_end Date.today - 1.week
  end
end
