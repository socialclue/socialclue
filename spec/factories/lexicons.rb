require "factory_girl"

FactoryGirl.define do

  factory :lexicons do
    ngram "today"
    score 2
    num_pos 10
    num_neg 10
  end

end
