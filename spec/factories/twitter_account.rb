require "factory_girl"

FactoryGirl.define do

  factory :twitter_account do
    name "test"
    provider "twitter"
    token 12345
    token_secret 123456
    uid 123456
    user "test@test.com"
  end

end
