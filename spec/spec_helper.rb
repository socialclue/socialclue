# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV["RAILS_ENV"] ||= 'test'
require File.expand_path("../../config/environment", __FILE__)
require 'rspec/rails'
require 'rspec/autorun'
require 'capybara/rspec'
require 'capybara/rails'
require 'webmock/rspec'
require 'stripe_mock'
require 'email_spec'

WebMock.disable_net_connect!(allow_localhost: true)

OmniAuth.config.test_mode = true
omniauth_hash = { 'provider' => 'twitter',
                  'uid' => '12345',
                  'info' => {
                      'name' => 'natasha'
                  },
                  'credentials' => {
                              'token' => '123456',
                              'secret' => '123456abc',
                  }
}

fb_omniauth_hash = {  'info' => {
                        'name' => 'Mario Brothers'
                       },
                      'uid' => '123545',
                      'provider' => 'facebook',
                      'credentials' => {
                        'token' => 'token',
                        'expires_at' => '2014-06-23 21:22:19'
                      }
                    }
OmniAuth.config.add_mock(:twitter, omniauth_hash)
OmniAuth.config.add_mock(:facebook, fb_omniauth_hash)

# Requires supporting ruby files with custom matchers and macros, etc,
# in spec/support/ and its subdirectories.
Dir[Rails.root.join("spec/support/**/*.rb")].each { |f| require f }

RSpec.configure do |config|
config.include(EmailSpec::Helpers)
config.include(EmailSpec::Matchers)

  config.before(:each) do
    WebMock.stub_request(:get, /auth/).
      with(headers: {'Accept'=>'*/*', 'User-Agent'=>'Ruby'}).
      to_return(status: 200, body: "stubbed response", headers: {})
  end

  config.before(:each) do
    WebMock.stub_request(:post, "https://api.stripe.com/v1/customers").
      with(:body => {"card"=>"test21token", "description"=>"test21@example.com", "email"=>"test21@example.com", "plan"=>"standard_plan"},
      :headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip, deflate', 'Authorization'=>'Bearer sk_test_4TYyyW24m8I5Ip0apvUSlwoD', 'Content-Length'=>'94', 'Content-Type'=>'application/x-www-form-urlencoded', 'User-Agent'=>'Stripe/v1 RubyBindings/1.14.0', 'X-Stripe-Client-User-Agent'=>'{"bindings_version":"1.14.0","lang":"ruby","lang_version":"2.0.0 p247 (2013-06-27)","platform":"x86_64-darwin12.4.1","publisher":"stripe","uname":"Darwin Bobs-MacBook-Air-2.local 13.3.0 Darwin Kernel Version 13.3.0: Tue Jun  3 21:27:35 PDT 2014; root:xnu-2422.110.17~1/RELEASE_X86_64 x86_64"}'}).
      to_return(:status => 200, :body => "", :headers => {})
  end

  config.before(:each) do
    stub_request(:get, "https://api.stripe.com/v1/customers/cus_4YmU4hxOFTbkAv").
      with(:headers => {'Accept'=>'*/*; q=0.5, application/xml', 'Accept-Encoding'=>'gzip, deflate', 'Authorization'=>'Bearer sk_test_4TYyyW24m8I5Ip0apvUSlwoD', 'Content-Type'=>'application/x-www-form-urlencoded', 'User-Agent'=>'Stripe/v1 RubyBindings/1.14.0', 'X-Stripe-Client-User-Agent'=>'{"bindings_version":"1.14.0","lang":"ruby","lang_version":"2.0.0 p247 (2013-06-27)","platform":"x86_64-darwin12.4.1","publisher":"stripe","uname":"Darwin Bobs-MacBook-Air-2.local 13.3.0 Darwin Kernel Version 13.3.0: Tue Jun  3 21:27:35 PDT 2014; root:xnu-2422.110.17~1/RELEASE_X86_64 x86_64"}'}).
      to_return(:status => 200, :body => "", :headers => {})
  end

  # Remove this line if you're not using ActiveRecord or ActiveRecord fixtures
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  # If you're not using ActiveRecord, or you'd prefer not to run each of your
  # examples within a transaction, remove the following line or assign false
  # instead of true.
  config.use_transactional_fixtures = true

  # If true, the base class of anonymous controllers will be inferred
  # automatically. This will be the default behavior in future versions of
  # rspec-rails.
  config.infer_base_class_for_anonymous_controllers = false

  # Run specs in random order to surface order dependencies. If you find an
  # order dependency and want to debug it, you can fix the order by providing
  # the seed, which is printed after each run.
  #     --seed 1234
  config.order = "random"

  config.include Capybara::DSL

  # Clean up the database
  config.before(:suite) do
    DatabaseCleaner.strategy = :truncation
    DatabaseCleaner.orm = "active_record"
    DatabaseCleaner.clean
  end

  config.before(:each) do
    DatabaseCleaner.clean
  end
end


