require "spec_helper"

describe StripeMailer do

  describe '#payment_received' do
    let(:user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      FactoryGirl.create(:invoice, user_id: user.id)
      user
    }

    let(:mail) { StripeMailer.payment_received(user, 1000, Date.today, (Date.today + 1.month)) }

    it "has the correct user email" do
      mail.to.should == [user.email]
    end

    it "has the correct senders email" do
      mail.from.should == ["info@socialclue.io"]
    end

    it "has the correct subject" do
      mail.subject.should == "Payment Received – Thank You"
    end
  end

  describe '#payment_failed' do
    let(:user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      FactoryGirl.create(:invoice, user_id: user.id)
      user
    }

    let(:mail) { StripeMailer.payment_failed(user, 1000, Date.today, (Date.today + 1.month)) }

    it "has the correct user email" do
      mail.to.should == [user.email]
    end

    it "has the correct senders email" do
      mail.from.should == ["info@socialclue.io"]
    end

    it "has the correct subject" do
      mail.subject.should == "Payment to SocialClue Failed"
    end
  end

  describe '#new_payment_information' do
    let(:user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      FactoryGirl.create(:invoice, user_id: user.id)
      user
    }

    let(:mail) { StripeMailer.new_payment_information(user) }

    it "has the correct user email" do
      mail.to.should == [user.email]
    end

    it "has the correct senders email" do
      mail.from.should == ["info@socialclue.io"]
    end

    it "has the correct subject" do
      mail.subject.should == "Updated Payment Information"
    end
  end

  describe '#account_cancelled' do
    let(:user) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      FactoryGirl.create(:invoice, user_id: user.id)
      user
    }

    let(:mail) { StripeMailer.account_cancelled(user) }

    it "has the correct user email" do
      mail.to.should == [user.email]
    end

    it "has the correct senders email" do
      mail.from.should == ["info@socialclue.io"]
    end

    it "has the correct subject" do
      mail.subject.should == "Your SocialClue Account has Been Cancelled"
    end
  end
end
