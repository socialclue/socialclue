require 'spec_helper'

describe "User edits their profile details" do

  describe "User changes their email" do
    context "with valid password" do
      it "successfully updates account" do
        sign_up_with "valid@example.com", "password"
        change_email
        user_sees_notice('You updated your account successfully.')
      end
    end
    context 'with invlaid password' do
      it "does not update account" do
        sign_up_with 'valid@example.com', "wrong password"
        change_email
        user_sees_notice('Current password is invalid')
      end
    end
  end

  describe "User changes their password" do
    context "with valid password" do
      it "successfully updates account" do
        sign_up_with "valid@example.com", "password"
        change_password
        user_sees_notice('You updated your account successfully.')
      end
    end
    context 'with invlaid password' do
      it "does not update account" do
        sign_up_with 'valid@example.com', "wrong password"
        change_password
        user_sees_notice('Current password is invalid')
      end
    end
  end
end
