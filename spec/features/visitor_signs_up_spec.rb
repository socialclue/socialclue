require 'spec_helper'

feature "Visitor signs up" do

  scenario "with valid email and password" do
    sign_up_with "valid@example.com", "password"
    user_sees_welcome_message("Welcome! You have signed up successfully.")
    user_should_be_signed_in
  end

  scenario 'with invalid email' do
    sign_up_with 'invalid_email', 'password'
    user_sees_notice("Email is invalid")
    user_should_be_signed_out
  end

  scenario 'with blank password' do
    sign_up_with 'valid@example.com', ""
    user_sees_notice("Password can't be blank")
    user_should_be_signed_out
  end

end
