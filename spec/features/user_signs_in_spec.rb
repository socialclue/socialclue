require 'spec_helper'

feature "User signs in" do

  scenario "with valid email and password" do
    sign_in
    user_sees_welcome_message("Signed in successfully.")
    user_should_be_signed_in
  end

  scenario 'with invalid email' do
    sign_in_with 'invalid_email', 'password'
    user_sees_notice('Invalid email or password.')
    user_should_be_signed_out
  end

  scenario 'with blank password' do
    sign_in_with 'valid@example.com', ""
    user_sees_notice('Invalid email or password.')
    user_should_be_signed_out
  end

end
