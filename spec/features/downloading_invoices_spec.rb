require 'spec_helper'

describe 'Downloading invoices' do

  context "for user without any subscription ever" do

    before { sign_in }

    it "should not show an invoices link" do
      navigate_to_user_account

      expect(page).not_to have_link("View invoices")
    end
  end

  context "for user with previous subscription" do

    let(:user_with_inactive_subscription) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, :subscription_expired, user_id: user.id)
      FactoryGirl.create(:invoice, user_id: user.id)
      user
    }

    before { sign_in_with_subscription user_with_inactive_subscription }

    it "should show invoices link" do
      navigate_to_user_account

      expect(page).to have_link("View invoices")
    end

    it "should allow user to download one of the listed invoices" do
      navigate_to_user_account

      click_on 'View invoices'

      expect(page).to have_link("invoice")

      click_on "invoice"

      expect(page.response_headers['Content-Type']).to eql("application/pdf")
    end
  end

  context "for user with an active subscription and invoices" do

    let(:user_with_active_subscription) {
      user = FactoryGirl.create(:user, :trial_has_expired)
      FactoryGirl.create(:subscription, user_id: user.id)
      FactoryGirl.create(:invoice, user_id: user.id)
      user
    }

    before { sign_in_with_subscription user_with_active_subscription }

    it "should show invoices link" do
      navigate_to_user_account

      expect(page).to have_link("View invoices")
    end

    it "should allow user to download one of the listed invoices" do
      navigate_to_user_account

      click_on 'View invoices'

      expect(page).to have_link("invoice")

      click_on "invoice"

      expect(page.response_headers['Content-Type']).to eql("application/pdf")
    end
  end
end
