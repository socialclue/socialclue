require 'spec_helper'

describe "User edits their account details" do

  context "user does not have an active paying subscription" do
    let(:user) { FactoryGirl.create(:user) }
    it "does not display paying account edit links" do
      sign_up_with "valid@example.com", "password"
      visit "/accounts/#{user.id}"
      expect(page).not_to have_link("Cancel subscription")
      expect(page).not_to have_link("Edit card details")
      expect(page).not_to have_link("View invoices")
    end
  end

  context "user has an active paying subscription" do
    let(:user_with_active_subscription) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, user_id: user.id)
        FactoryGirl.create(:invoice, user_id: user.id)
        user
      }
    it "displays paying account edit links" do
      sign_in_with_subscription user_with_active_subscription
      visit "/accounts/#{user_with_active_subscription.id}"
      expect(page).to have_link("Cancel subscription")
      expect(page).to have_link("Edit card details")
      expect(page).to have_link("View invoices")
    end
  end

  context "user has an inactive paying subscription" do
    let(:user_with_inactive_subscription) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, :subscription_expired, user_id: user.id)
        FactoryGirl.create(:invoice, user_id: user.id)
        user
      }
    it "does not display paying account edit links" do
      sign_in_with_subscription user_with_inactive_subscription
      visit "/accounts/#{user_with_inactive_subscription.id}"
      expect(page).not_to have_link("Cancel subscription")
      expect(page).not_to have_link("Edit card details")
      expect(page).to have_link("View invoices")
    end
  end
end
