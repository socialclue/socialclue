require 'spec_helper'

describe "Visitor navigates to pricing page" do

  it "clicks CTA for free trial" do
    visit '/pages/pricing'
    click_link 'Sign up - 14 day free trial'
    expect(page).not_to have_content("No credit card required!")
    expect(page).to have_button("Sign up")
  end
end
