require 'spec_helper'

describe "browsing dashboard" do

  context "user is not signed in" do
    it "redirects to sign in page" do
      visit '/dashboard'
      expect(page).to have_content("You need to sign in or sign up before continuing.")
    end
  end

  context "user is signed in" do
    context "trial is active" do
      context "visits dashboard page" do
        before do
          sign_in
          navigate_to_dashboard
        end

        it "shows twitter pannel" do
          within(".well-lg.twitter") { expect(page).to have_content("Twitter") }
        end

        it "shows facebook pannel" do
          within(".well-lg.facebook") { expect(page).to have_content("Facebook") }
        end
      end
    end

    context "trial has expired" do
      context "visits dashboard page" do
        before do
          sign_in_trial_expired
          navigate_to_dashboard
        end

        it "redirects to new subscription page" do
          user_sees_notice("Your subscription is no longer active please upgrade to continue")
        end
      end
    end

    context "subscription is active" do
      let(:user_with_active_subscription) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, user_id: user.id)
        user
      }
      context "visits dashboard page" do
        before do
          sign_in_with_subscription user_with_active_subscription
          navigate_to_dashboard
        end

        it "shows twitter pannel" do
          within(".well-lg.twitter") { expect(page).to have_content("Twitter") }
        end

        it "shows facebook pannel" do
          within(".well-lg.facebook") { expect(page).to have_content("Facebook") }
        end
      end
    end

    context "subscription is inactive" do
      let(:user_with_inactive_subscription) {
        user = FactoryGirl.create(:user, :trial_has_expired)
        FactoryGirl.create(:subscription, :subscription_expired, user_id: user.id)
        user
      }
      context "visits dashboard page" do
        before do
          sign_in_with_subscription user_with_inactive_subscription
          navigate_to_dashboard
        end

        it "redirects to new subscription page" do
          user_sees_notice("Your subscription is no longer active please upgrade to continue")
        end
      end
    end
  end
end
