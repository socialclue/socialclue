module Features
  module UserSessionHelpers

    def sign_up_with(email, password)
      visit new_user_registration_path
      fill_in 'user[email]', with: email
      fill_in 'user[password]', with: password
      fill_in 'user[password_confirmation]', with: password
      click_button 'Sign up'
    end

    def sign_in_with(email, password)
      visit new_user_session_path
      fill_in 'user[email]', with: email
      fill_in 'user[password]', with: password
      click_button 'Sign in'
    end

    def sign_in
      visit '/users/sign_in'
      fill_in 'user[email]', with: FactoryGirl.create(:user).email
      fill_in 'user[password]', with: FactoryGirl.create(:user).password
      click_button 'Sign in'
    end

    def sign_in_trial_expired
      user = FactoryGirl.create(:user, :trial_has_expired)
      visit '/users/sign_in'
      fill_in 'user[email]', with: user.email
      fill_in 'user[password]', with: user.password
      click_button 'Sign in'
    end

    def sign_in_with_subscription user
      visit '/users/sign_in'
      fill_in 'user[email]', with: user.email
      fill_in 'user[password]', with: user.password
      click_button 'Sign in'
    end

    def user_should_be_signed_in
      expect(page).to have_content("Sign out")
    end

    def user_should_be_signed_out
      expect(page).to have_content('Sign in')
    end

    def change_email
      visit "/users/edit"
      fill_in 'user[email]', with: "new_eamil@email_new.com"
      fill_in 'user[current_password]', with: 'password'
      click_button 'Update'
    end

    def change_password
      visit "/users/edit"
      fill_in 'user[email]', with: "valid@example.com"
      fill_in 'user[password]', with: "new_password"
      fill_in 'user[password_confirmation]', with: "new_password"
      fill_in 'user[current_password]', with: 'password'
      click_button 'Update'
    end

    def user_sees_notice(text)
      expect(page).to have_content text
    end

    def user_sees_welcome_message(text)
      expect(page).to have_content text
    end

    def navigate_to_dashboard
      visit '/'
      click_on 'Dashboard'
    end

    def navigate_to_user_account
      click_on 'Account'
    end

  end
end
