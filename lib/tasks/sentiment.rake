namespace :sentiment do
  desc "Process words for twitter analytics"
  task bigram_lexicon: :environment do
    file_path = File.expand_path("../lexicon_bigram.txt", __FILE__)
    File.open(file_path, "r").each do |line|
      ngram_one, ngram_two, score, num_pos, num_neg = line.strip.split(" ")
      ngram = ngram_one + " " + ngram_two
      analytics = Lexicons.where(:ngram => ngram).first_or_create
      analytics.update_attributes({
        :ngram => ngram,
        :score => score,
        :num_pos => num_pos,
        :num_neg => num_neg
        })
    end
  end
  task unigram_lexicon_large: :environment do
    file_path = File.expand_path("../lexicon_unigram_large.txt", __FILE__)
    File.open(file_path, "r").each do |line|
      ngram, score, num_pos, num_neg = line.strip.split(" ")
      analytics = Lexicons.where(:ngram => ngram).first_or_create
      analytics.update_attributes({
        :ngram => ngram,
        :score => score,
        :num_pos => num_pos,
        :num_neg => num_neg
        })
    end
  end
  task unigram_lexicon_small: :environment do
    file_path = File.expand_path("../lexicon_unigram_small.txt", __FILE__)
    File.open(file_path, "r").each do |line|
     ngram, rank, score, std_dev, twitter, google, nyt, lyrics = line.strip.split(" ")
     score = score.to_f - 5
      analytics = Lexicons.where(:ngram => ngram).first_or_create
      analytics.update_attributes({
        :ngram => ngram,
        :score => score,
        :num_pos => nil,
        :num_neg => nil
        })
    end
  end
end




