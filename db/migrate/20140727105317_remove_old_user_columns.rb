class RemoveOldUserColumns < ActiveRecord::Migration
  def up
    remove_column :users, :provider
    remove_column :users, :name
    remove_column :users, :token
    remove_column :users, :token_secret
    remove_column :users, :uid
    remove_column :users, :user_id
  end

  def down
    remove_column :users, :provider, :string
    remove_column :users, :name, :string
    remove_column :users, :token, :string
    remove_column :users, :token_secret, :string
    remove_column :users, :uid, :string
    remove_column :users, :user_id, :integer
  end
end
