class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.integer :user_id
      t.string :default_card
      t.string :stripe_id
      t.datetime :active_until
      t.boolean :active, :default => false

      t.timestamps
    end
  end
end
