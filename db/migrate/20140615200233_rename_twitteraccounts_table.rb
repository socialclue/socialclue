class RenameTwitteraccountsTable < ActiveRecord::Migration
  def change
    rename_table :twitteraccounts, :twitter_accounts
  end
end
