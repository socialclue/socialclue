class AddActiveFromToSubscriptions < ActiveRecord::Migration
  def change
    add_column :subscriptions, :active_from, :datetime
  end
end
