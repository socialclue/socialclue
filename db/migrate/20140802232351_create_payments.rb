class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.integer :user_id
      t.string :amount
      t.string :stripe_reference_id

      t.timestamps
    end
  end
end

