class AddTrialingToUsers < ActiveRecord::Migration
  def change
    add_column :users, :trial_start, :datetime
    add_column :users, :trial_end, :datetime
  end
end
