class ChangeTwitterAccountUserColumnType < ActiveRecord::Migration
  def change
   change_column :twitter_accounts, :user, :string
  end
end
