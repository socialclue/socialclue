class RenamePaymentsToInvoices < ActiveRecord::Migration
  def change
    rename_table :payments, :invoices
  end
end
