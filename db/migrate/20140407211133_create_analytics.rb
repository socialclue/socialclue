class CreateAnalytics < ActiveRecord::Migration
  def change
    create_table :analytics do |t|
      t.string :ngram
      t.string :score
      t.string :num_pos
      t.string :num_neg

      t.timestamps
    end
  end
end
