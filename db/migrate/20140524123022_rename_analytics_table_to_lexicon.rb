class RenameAnalyticsTableToLexicon < ActiveRecord::Migration
  def change
    rename_table :analytics, :lexicon
  end
end
