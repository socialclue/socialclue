class CreateTwitteraccounts < ActiveRecord::Migration
  def change
    create_table :twitteraccounts do |t|
      t.string :provider
      t.string :uid
      t.string :name
      t.string :token
      t.string :token_secret

      t.timestamps
    end
  end
end
