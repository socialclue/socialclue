class RenameTwitteraccountColumnUserid < ActiveRecord::Migration
  def change
    rename_column :twitteraccounts, :user_id, :user
  end
end
