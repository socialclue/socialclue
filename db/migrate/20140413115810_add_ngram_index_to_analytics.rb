class AddNgramIndexToAnalytics < ActiveRecord::Migration
  def change
    add_index :analytics, :ngram, :unique => true
  end
end
