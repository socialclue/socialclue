# SocialClue

[ ![Codeship Status for SocialClue/SocialClue official](https://www.codeship.io/projects/0e6cbb00-d6bb-0131-3dc4-6eee351bfe7d/status)](https://www.codeship.io/projects/23823)

## Application Architecture

SocialClue is using:

* Rails 3.2.14
* Ruby 2.0.0-p247
* Postgres DB
* Server Unicorn
* Vagrant

## Getting up and running

Create the vagrant VM:

  $ vagrant up

Install gems

    $ bundle

### Database

SocialClue uses Postgresql, so you need to have that installed when you want to run it or any of the tests.  Make sure that config/database.yml is configured with your particular database setup, if it differs from the default.

When configured correctly, you can run this to create the database:

    $ bundle exec rake db:setup

And this to ensure the test database is setup correctly too:

    $ bundle exec rake db:test:prepare

### Lexicons

Run the following rake tasks to upload the lexicons. Without these, sentiment analysis will not work.

    $ bundle exec rake sentiment:unigram_lexicon_small
    $ bundle exec rake sentiment:unigram_lexicon_large
    $ bundle exec rake sentiment:bigram_lexicon

## Testing

### Local testing

First, make sure the test database is set up the same as the database in development (if not already done):

    $ bundle exec rake db:test:prepare

Specs

    $ bundle exec rake


### Continuous Integration

We're currently using CodeShip to continually test and deploy the project.

CodeShip - https://www.codeship.io/projects/23823

Codeship will automatically run tests on any branch once it's pushed to bitbucket. Results can be viewed at above address.

## Deploying

SocialClue is currently hosted on Heroku:

* Staging - http://staging-socialclue.herokuapp.com/
* Production - http://socialclue.herokuapp.com/
* Production - https://www.socialclue.io/

When Codeship is enabled in Bitbucket, all code pushed to any branch on bitbucket will be tested and, considering all tests pass, automatically deployed to the SocialClue-staging Heroku app.
