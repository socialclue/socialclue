Rails.configuration.stripe = {
  :publishable_key => ENV["STRIPE_PUBLIC_KEY"],
  :secret_key      => ENV["STRIPE_API_KEY"]
}
Stripe.api_key = Rails.configuration.stripe[:secret_key]
STRIPE_PUBLIC_KEY = Rails.configuration.stripe[:publishable_key]

StripeEvent.configure do |events|
  events.subscribe 'charge.succeeded', ChargeSucceeded.new
  events.subscribe 'invoice.payment_failed', PaymentFailed.new
  events.subscribe 'customer.subscription.deleted', SubscriptionDeleted.new
  events.subscribe 'customer.updated', CustomerUpdated.new
end
