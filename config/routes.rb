SocialClue::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :users, controllers: {
    registrations: 'users/registrations',
  }

  mount StripeEvent::Engine => '/stripe-webhooks'

  match 'auth/facebook/callback', to: 'sessions#create_facebook', via: [:get, :post]
  get '/auth/twitter/callback', to: 'sessions#create_twitter'

  delete '/appauth/signout', to: 'sessions#destroy'
  match 'auth/failure', to: redirect('/'), via: [:get, :post]

  resources :home, :only => [:index]

  get '/dashboard', to: 'dashboard#index'

  resources :subscriptions, only: [:new, :create] do
    put 'cancel', on: :collection
  end
  match 'subscriptions/edit',   to: 'subscriptions#edit',   via: 'get'
  match 'subscriptions/update', to: 'subscriptions#update', via: 'put'

  resources :pages, :only => [:about, :faq] do
    collection do
      get 'about'
      get 'learn_more'
      get 'privacy_policy'
      get 'terms_of_service'
      get 'pricing'
    end
  end

  namespace :twitter do
    resources :analytics, :only => [:start, :sentiment, :stats, :keywords] do
      collection do
        get 'start'
        get 'sentiment'
        get 'stats'
        get 'keywords'
      end
    end
    resources :photos, :only => [:search] do
      get 'search', on: :collection
    end
    resources :info, :only => [:about] do
      get 'about', on: :collection
    end
    resources :geo_search, :only => [:sentiment, :stats, :keywords, :search] do
      collection do
        get 'sentiment'
        get 'stats'
        get 'keywords'
        get 'search'
      end
    end
  end

  namespace :facebook do
    resources :analytics, :only => [:start, :search, :feed, :stats, :keywords] do
      collection do
        get 'start'
        get 'search'
        get 'feed'
        get 'stats'
        get 'keywords'
      end
    end
  end

  resources :invoices, only: [:index, :show]

  resources :accounts, only: [:show]

  get '/404', to: 'errors#not_found'
  get '/500', to: 'errors#server_error'

  root :to => 'home#index'

end
